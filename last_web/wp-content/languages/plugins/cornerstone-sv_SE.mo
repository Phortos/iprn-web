��    >        S   �      H     I     ]     e     m     {     �     �     �     �  
   �     �     �     �     �  L   �     I     N     T  
   `     k     r     �     �     �     �  O   �       S        p     �  S   �     �     �  #   �          %  
   4     ?     X     n     �     �     �     �  K   �     0	     <	     P	  ;   b	     �	  (   �	     �	     �	  
   
     
     $
     1
     @
  
   O
  M   Z
  W   �
  !        "     =     E     M     `     r     �     �     �  
   �     �     �     �     �  M   �     <  	   E     O  
   `  	   k     u     �  %   �     �      �  N   �     >  V   N     �     �  U   �          /  "   <     _     g     w  "   �     �     �     �     �          ;  W   W     �     �     �  R   �     :  (   P     y     �     �     �     �  
   �  
   �     �  I   �  Y           <            5          #          2                       
   =   4   6         9       >   ,                       (   ;   "                 -      *   +          .   :   1   $   8              !   	                     3       0                         /       '   7   )       %      &          %1$s %2$s %3$s Feed &laquo; &raquo; - No Change - - No Section - Add New Add New  All %s Apply Archetyped Bulk Actions Cornerstone Delete Permanently Displaying %s&#8211;%s of %s Draft <span class="count">(%s)</span> Drafts <span class="count">(%s)</span> Edit Edit  Empty Trash Extra Menu Filter Move to Trash No posts found No posts found in the trash None Pending posts Private <span class="count">(%s)</span> Private <span class="count">(%s)</span> Private posts Published <span class="count">(%s)</span> Published <span class="count">(%s)</span> Published posts Restore Scheduled <span class="count">(%s)</span> Scheduled <span class="count">(%s)</span> Scheduled posts Search Posts Search results for &#8220;%s&#8221; Section Show all dates Structured Subscribe to All updates Subscribe to Comments Subscribe to Updates Subscribe to this Search Subscribe to this Section Subscribe to this Tag Subscribe to this Topic Trash <span class="count">(%s)</span> Trash <span class="count">(%s)</span> Trash posts View all categories View all sections You do not have sufficient permissions to access this page. http://archetyped.com http://archetyped.com/tools/cornerstone/ manage posts headerDrafts postAdd New postDraft postPending Review postPrivate postPublished postScheduled postTrash postsAll <span class="count">(%s)</span> All <span class="count">(%s)</span> postsMy Posts <span class="count">(%s)</span> My Posts <span class="count">(%s)</span> PO-Revision-Date: 2020-02-28 06:44:26+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: sv_SE
Project-Id-Version: Plugins - Cornerstone - Development (trunk)
 Flöde för %1$s %2$s %3$s &laquo; &raquo; - Ingen ändring - - Ingen sektion - Lägg till nytt Lägg till ny  Alla %s Kör Archetyped Massåtgärder Cornerstone Ta bort permanent Visar %s&#8211;%s av %s Utkast <span class="count">(%s)</span> Utkast <span class="count">(%s)</span> Redigera Redigera  Töm papperskorg Extra meny ﻿Filter Flytta till papperskorg Inga inlägg hittades Inga inlägg hittades i papperskorgen Inga Inlägg som inväntar granskning Privat <span class="count">(%s)</span> Privata <span class="count">(%s)</span> Privata inlägg Publicerad <span class="count">(%s)</span> Publicerade <span class="count">(%s)</span> Publicerade inlägg Återställ Scemalagd <span class="count">(%s)</span> Schemalagda <span class="count">(%s)</span> Schemalagda inlägg Sök inlägg Sökresultat för &#8220;%s&#8221; Sektion Visa alla datum Strukturerad Prenumerera på alla uppdateringar Prenumerera på kommentarer Prenumerera på uppdateringar Prenumerera på dena sökning Prenumerera på denna sektion Prenumerera på denna etikett Prenumerera på detta ämne Papperskorg <span class="count">(%s)</span> Papperskorg <span class="count">(%s)</span> Släng inlägg Visa alla kategorier Visa alla sektioner Du har inte tillräckliga rättigheter för att få tillgång till den här sidan. http://archetyped.com http://archetyped.com/tools/cornerstone/ Utkast Lägg till ny Utkast Inväntar granskning Privat Publicerat Schemalagd Ta bort Alla <span class="count">(%s)</span> Alla <span class="count">(%s)</span> Mitt inlägg <span class="count">(%s)</span> Mina inlägg <span class="count">(%s)</span> 