��    A      $  Y   ,      �     �     �     �     �     �     �     �     �     �  
   �     �               '  L   D     �     �     �  )   �  
   �     �     �     �            ]   "     �  O   �     �  S   �     @     P  S   X     �     �  #   �     �     �  
   	     	     (	     >	     S	     l	     �	     �	  K   �	      
     
      
  %   2
  ;   X
     �
  (   �
     �
     �
  
   �
               '     6  
   E  M   P  W   �    �          )     1     9     H     ]     k     z     �  
   �     �     �     �     �  S   �     =     D     L  .   _     �     �     �     �  *   �     �  o         p  Q   �     �  T   �     =  	   Q  V   [     �     �  -   �               '  '   4     \  !   z     �     �     �     �  Q   	     [     p     �  +   �  5   �       (     
   C     N     \     e     |  	   �  
   �     �  K   �  Y   �             :                             /          #   7   	      $   0   (                           2      '   )   6                        1   =          8      *   3           ,   "                    ?       5      &   @   9           A   ;   %          +       
   .               -   !       4      <      >             %1$s %2$s %3$s Feed &laquo; &raquo; - No Change - - No Section - Add New Add New  All %s Apply Archetyped Bulk Actions Cornerstone Delete Permanently Displaying %s&#8211;%s of %s Draft <span class="count">(%s)</span> Drafts <span class="count">(%s)</span> Edit Edit  Empty Trash Enhanced content management for WordPress Extra Menu Filter Move to Trash No posts found No posts found in the trash None Pending Review <span class="count">(%s)</span> Pending Review <span class="count">(%s)</span> Pending posts Private <span class="count">(%s)</span> Private <span class="count">(%s)</span> Private posts Published <span class="count">(%s)</span> Published <span class="count">(%s)</span> Published posts Restore Scheduled <span class="count">(%s)</span> Scheduled <span class="count">(%s)</span> Scheduled posts Search Posts Search results for &#8220;%s&#8221; Section Show all dates Structured Subscribe to All updates Subscribe to Comments Subscribe to Updates Subscribe to this Search Subscribe to this Section Subscribe to this Tag Subscribe to this Topic Trash <span class="count">(%s)</span> Trash <span class="count">(%s)</span> Trash posts View all categories View all sections You are not allowed to edit this item You do not have sufficient permissions to access this page. http://archetyped.com http://archetyped.com/tools/cornerstone/ manage posts headerDrafts postAdd New postDraft postPending Review postPrivate postPublished postScheduled postTrash postsAll <span class="count">(%s)</span> All <span class="count">(%s)</span> postsMy Posts <span class="count">(%s)</span> My Posts <span class="count">(%s)</span> PO-Revision-Date: 2020-02-28 02:38:29+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: es
Project-Id-Version: Plugins - Cornerstone - Development (trunk)
 %1$s %2$s %3$s Feed &laquo; &raquo; - Sin cambio - - Ninguna sección - Añadir nueva Añadir nueva  Todas los %s Aplicar Archetyped Acciones en lote Cornerstone Borrar permanentemente Mostrando %s&#8211;%s de%s Borrador <span class="count">(%s)</span> Borradores <span class="count">(%s)</span> Editar Editar  Vaciar la Papelera Gestión mejorada de contenidos para WordPress Menú extra Filtro Mover a papelera No se encontraron entradas No se han encontrado entradas en la basura Ninguna Pendientes de revisión <span class="count">(%s)</span> Pendientes de revisión <span class="count">(%s)</span> Entradas pendientes Privados <span class="count">(%s)</span> Privados <span class="count">(%s)</span> Entradas privadas Publicada <span class="count">(%s)</span> Publicadas <span class="count">(%s)</span> Entradas publicadas Restaurar Programada <span class="count">(%s)</span> Programadas <span class="count">(%s)</span> Entradas programadas Buscar entradas Resultados para la búsqueda &#8220;%s&#8221; Sección Mostrar todas las fechas Estructurado Suscribirse a todas las actualizaciones Suscribirse a los comentarios Suscribirse a las actualizaciones Suscribirse a esta búsqueda Suscribirse a esta sección Suscribirse a esta etiqueta Suscribirse a este tema Papelera <span class="count">(%s)</span> Papelera <span class="count">(%s)</span> Enviar a la papelera Ver todas las categorías Ver todas las secciones No tienes permiso para editar este elemento No tienes suficientes permisos para ver esta página. http://archetyped.com http://archetyped.com/tools/cornerstone/ Borradores Añadir nueva Borrador Pendiente de revisión Privada Publicada Programada Papelera Todas <span class="count">(%s)</span> Todas <span class="count">(%s)</span> Mis entradas <span class="count">(%s)</span> Mis entradas <span class="count">(%s)</span> 