
<?php
um_fetch_user(get_post_meta(get_the_ID(),'member_id', true));
	// echo apply_filters('the_content', get_the_content());

	$photo = um_user('cover_photo');
	if (preg_match('/<img.+?src(?:)*=(?:)*[\'"](.*?)[\'"]/si', $photo, $arrResult)) {
		$cover = $arrResult[1];
	} else {
		$cover = '';
	}
	$cover = getsrc($photo);
	
	$content = get_the_content();
	$content = preg_replace('/(.*?)(http:\/\/iprn.overtek.es\/wp-content\/uploads\/2017\/07\/header_corporate_communications.png)(.*?)/', '${1}'.$cover.'${3}', $content);
	// CAMBIO TITULO
	$content = preg_replace('/(\[et_pb_text.*?module_id=\"titulo\".*?\])(.*?)(\[\/et_pb_text\])/', '${1}'.um_user('display_name').'${3}', $content);
	// CAMBIO DESCRIPCION
	$content = preg_replace('/(\[et_pb_text.*?module_id=\"descripcion\".*?\])(.*?)(\[\/et_pb_text\])/', '${1}'.um_user('description').'${3}', $content);

	// CAMBIO RESUMEN
	$content = preg_replace('/(\[et_pb_text.*?module_id=\"resumen\".*?\])(.*?)(\[\/et_pb_text\])/', '${1}'.um_user('summary_id').'${3}', $content);
	// CAMBIO COMPNAY
	$content = preg_replace('/(\[et_pb_blurb.*?module_id=\"company\".*?\])(.*?)(\[\/et_pb_blurb\])/', '${1}'.um_user('companyname_id').'${3}', $content);
	
	// CAMBIO EMAIL 
	$content = preg_replace('/(\[et_pb_blurb.*?module_id=\"email\".*?\])(.*?)(\[\/et_pb_blurb\])/', '${1}'.um_user('Primary_email').'${3}', $content);
	
	// CAMBIO LOCATION
	$content = preg_replace('/(\[et_pb_blurb.*?module_id=\"location\".*?\])(.*?)(\[\/et_pb_blurb\])/', '${1}'.um_user('location_city').' ('.um_user('country').')'.'${3}', $content);
	// CAMBIO sectors 
	$sectors = join(', ', um_user('Specialisms_36'));
	$content = preg_replace('/(\[et_pb_text.*?module_id=\"sectors\".*?\])(.*?)(\[\/et_pb_text\])/', '${1}'.$sectors.'${3}', $content);
	// CAMBIO web
	$content = preg_replace('/(\[et_pb_blurb.*?module_id=\"web\".*?\])(.*?)(\[\/et_pb_blurb\])/', '${1}'.um_user('Web').'${3}', $content);

	// CAMBIO CONTACTOS
	$firstContact = '<p style="font-weight:bold">'.um_user('contactname_id').'</p>'.'<p>'.um_user('Primary_email').'</p>';
	$secondContact = '<p style="font-weight:bold">'.um_user('seccondcontactinagency').'</p>'.'<p>'.um_user('Secondary_email').'</p>';
	$content = preg_replace('/(\[et_pb_blurb.*?module_id=\"firstContact\".*?\])(.*?)(\[\/et_pb_blurb\])/', '${1}'.$firstContact.'${3}', $content);
	$content = preg_replace('/(\[et_pb_blurb.*?module_id=\"secondContact\".*?\])(.*?)(\[\/et_pb_blurb\])/', '${1}'.$secondContact.'${3}', $content);
	

	// CAMBIO ADRESS
	$content = preg_replace('/(\[et_pb_blurb.*?module_id=\"adress\".*?\])(.*?)(\[\/et_pb_blurb\])/', '${1}'.um_user('Address_one').'${3}', $content);
	$content = preg_replace('/(\[et_pb_blurb.*?module_id=\"adressContact\".*?\])(.*?)(\[\/et_pb_blurb\])/', '${1}'.um_user('Address_two').'${3}', $content);
	// MORE INFO
	$content = preg_replace('/(\[et_pb_blurb.*?module_id=\"phone\".*?\])(.*?)(\[\/et_pb_blurb\])/', '${1}'.um_user('phone_number').'${3}', $content);
	$content = preg_replace('/(\[et_pb_blurb.*?module_id=\"skypeId\".*?\])(.*?)(\[\/et_pb_blurb\])/', '${1}'.um_user('skype').'${3}', $content);
	$content = preg_replace('/(\[et_pb_blurb.*?module_id=\"moreOffices\".*?\])(.*?)(\[\/et_pb_blurb\])/', '${1}'.um_user('additional_countries').'${3}', $content);
	$content = preg_replace('/(\[et_pb_text.*?module_id=\"prizes\".*?\])(.*?)(\[\/et_pb_text\])/', '${1}'.um_user('Prizes_won').'${3}', $content);

	// LAST SECTION
	$content = preg_replace('/(\[et_pb_text.*?module_id=\"staff\".*?\])(.*?)(\[\/et_pb_text\])/', '${1}'.um_user('Staff').'${3}', $content);
	$content = preg_replace('/(\[et_pb_text.*?module_id=\"sectors2\".*?\])(.*?)(\[\/et_pb_text\])/', '${1}'.um_user('sectors_top').'${3}', $content);
	$content = preg_replace('/(\[et_pb_text.*?module_id=\"expertSectors\".*?\])(.*?)(\[\/et_pb_text\])/', '${1}'.um_user('global_pr').'${3}', $content);

	$content = preg_replace('/(\[et_pb_text.*?module_id=\"owner\".*?\])(.*?)(\[\/et_pb_text\])/', '${1}'.um_user('Owner').'${3}', $content);
	$content = preg_replace('/(\[et_pb_blurb.*?module_id=\"revenues\".*?\])(.*?)(\[\/et_pb_blurb\])/', '${1}'.um_user('Revenues').'${3}', $content);
	$content = preg_replace('/(\[et_pb_blurb.*?module_id=\"sizeInCountry\".*?\])(.*?)(\[\/et_pb_blurb\])/', '${1}'.um_user('size').'${3}', $content);
	$content = preg_replace('/(\[et_pb_text.*?module_id=\"topClients\".*?\])(.*?)(\[\/et_pb_text\])/', '${1}'.um_user('Top_clients').'${3}', $content);


	// FOTOS
	$fotos = ['Photo_album', 'Photo_album_35', 'Photo_album_35_38', 'Photo_album_36', 'Photo_album_36_39', 'Photo_album_37'];
	$grid = '<div id="photos-container" class="grid">
	';
	
	foreach ($fotos as $foto) {
		
		$foto = um_user($foto);
		if ($foto != null) {
			$thumb = '/wp-content/uploads/ultimatemember/'.get_post_meta(get_the_ID(),'member_id', true).'/'.$foto;
			$grid.= '
			<a style="margin-bottom:5px" class="et_pb_gallery_image gallery-item" href="'.$thumb.'" >
				<div >
					<img width="200" src="'.$thumb.'" alt="client_03" style="margin-bottom: -1px;">
					<span class="et_overlay"></span>
					</div>
			</a>';
		}
		
	}
	$grid .= '</div>';
	if (um_user('Photo_album') != null) {
		$content = preg_replace('/(\[galleryMember\])/', $grid, $content);
	} else {
		$content = preg_replace('/(\[galleryMember\])/', '<div class="et_pb_text_inner" style="text-align: center;
    font-size: 20px;">User has not uploaded photos yet</div>', $content);
	}
					

	echo apply_filters('the_content', $content);
	if ( ! $is_page_builder_used )
		wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
?>
					