<?php


class Custom_ET_Builder_Module_Blog extends ET_Builder_Module_Blog {
  
	public function multi_view_filter_value( $raw_value, $args, $multi_view ) {
		$name    = isset( $args['name'] ) ? $args['name'] : '';
		$mode    = isset( $args['mode'] ) ? $args['mode'] : '';
		$context = isset( $args['context'] ) ? $args['context'] : '';

		if ( 'post_content' === $name && 'content' === $context ) {
			global $et_pb_rendering_column_content;

			$post_content = et_strip_shortcodes( et_delete_post_first_video( get_the_content() ), true );

			$et_pb_rendering_column_content = true;

			if ( 'on' === $raw_value ) {
				global $more;

				if ( et_pb_is_pagebuilder_used( get_the_ID() ) ) {
					$more = 1; // phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
					$raw_value = et_core_intentionally_unescaped( apply_filters( 'the_content', $post_content ), 'html' );
				} else {
					$more = null; // phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
					$raw_value = et_core_intentionally_unescaped( apply_filters( 'the_content', et_delete_post_first_video( get_the_content( esc_html__( 'read more...', 'et_builder' ) ) ) ), 'html' );
				}
			} else {
				$use_manual_excerpt = isset( $this->props['use_manual_excerpt'] ) ? $this->props['use_manual_excerpt'] : 'off';
				$excerpt_length     = isset( $this->props['excerpt_length'] ) ? $this->props['excerpt_length'] : 270;

				if ( has_excerpt() && 'off' !== $use_manual_excerpt ) {
					/**
					 * Filters the displayed post excerpt.
					 *
					 * @since 3.29
					 *
					 * @param string $post_excerpt The post excerpt.
					 */
					$raw_value = apply_filters( 'the_excerpt', get_the_excerpt() );
				} else {
					$raw_value = et_core_intentionally_unescaped( wpautop( et_delete_post_first_video( strip_shortcodes( truncate_post( $excerpt_length, false, '', true ) ) ) ), 'html' );
				}
			}

			$et_pb_rendering_column_content = false;
		} else if ( 'show_content' === $name && 'visibility' === $context ) {
			$raw_value = $multi_view->has_value( $name, 'on', $mode, true ) ? 'on' : $raw_value;
		} else if ( 'post_meta_removes' === $name && 'content' === $context ) {
			$post_meta_remove_keys = array(
				'show_author'     => true,
				'show_date'       => true,
				'show_categories' => true,
				'show_comments'   => true,
			);

			$post_meta_removes = explode( ',', $raw_value );

			if ( $post_meta_removes ) {
				foreach ( $post_meta_removes as $post_meta_remove ) {
					unset( $post_meta_remove_keys[ $post_meta_remove ] );
				}
			}

			$post_meta_datas = array();
			$pais = get_post_meta(get_the_ID(),'country', true);
			if ( isset( $post_meta_remove_keys['show_author'] ) ) {
				$post_meta_datas[] = et_get_safe_localization( sprintf( __( 'by %s', 'et_builder' ), '<span class="author vcard">' . et_pb_get_the_author_posts_link() . '</span>' ) );
			}
			if ($pais) {
				$post_meta_datas[] = '<span">'.$pais.'</span>';
			}
			if ( isset( $post_meta_remove_keys['show_date'] ) ) {
				$post_meta_datas[] = et_get_safe_localization( sprintf( __( '%s', 'et_builder' ), '<span class="published">' . esc_html( get_the_date( $this->props['meta_date'] ) ) . '</span>' ) );
			}

			if ( isset( $post_meta_remove_keys['show_categories'] ) ) {
				$post_meta_datas[] = et_builder_get_the_term_list( ', ' );
			}

			if ( isset( $post_meta_remove_keys['show_comments'] ) ) {
				$post_meta_datas[] = sprintf( esc_html( _nx( '%s Comment', '%s Comments', get_comments_number(), 'number of comments', 'et_builder' ) ), number_format_i18n( get_comments_number() ) );
			}

			$raw_value = implode( ' | ', $post_meta_datas );
		}

		return $raw_value;
	}
}

new ET_Builder_Module_Blog;
