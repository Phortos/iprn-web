<?php
/*
Template Name: Blank Page
*/

get_header();

$post_id              = get_the_ID();
$is_page_builder_used = et_pb_is_pagebuilder_used( $post_id );
$container_tag        = 'product' === get_post_type( $post_id ) ? 'div' : 'article'; ?>

    <div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<<?php echo $container_tag; ?> id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="main_title"><?php the_title(); ?></h1>
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$alttext = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
					$thumbnail = get_thumbnail( $width, $height, $classtext, $alttext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
					
					// echo apply_filters('the_content', get_the_content());
					$content = get_the_content();
					preg_match('/(\[et_pb_gallery gallery_ids=\"(?<galleryIds>.*?)\".*?module_id=\"24general\".*?\]).*?/', $content,$matches);
					$general24 = explode(',', $matches['galleryIds']);
					$content = preg_replace('/(\[et_pb_gallery gallery_ids=\"(?<galleryIds>.*?)\".*?module_id=\"24general\".*?\]\[\/et_pb_gallery\]).*?/', '', $content);
					preg_match('/(\[et_pb_gallery gallery_ids=\"(?<galleryIds>.*?)\".*?module_id=\"2018european\".*?\]).*?/', $content,$matches);
					$european2018 = explode(',', $matches['galleryIds']);
					$content = preg_replace('/(\[et_pb_gallery gallery_ids=\"(?<galleryIds>.*?)\".*?module_id=\"2018european\".*?\]\[\/et_pb_gallery\]).*?/', '', $content);
					preg_match('/(\[et_pb_gallery gallery_ids=\"(?<galleryIds>.*?)\".*?module_id=\"23general\".*?\]).*?/', $content,$matches);
					$general23 = explode(',', $matches['galleryIds']);
					$content = preg_replace('/(\[et_pb_gallery gallery_ids=\"(?<galleryIds>.*?)\".*?module_id=\"23general\".*?\]\[\/et_pb_gallery\]).*?/', '', $content);
					preg_match('/(\[et_pb_gallery gallery_ids=\"(?<galleryIds>.*?)\".*?module_id=\"2017european\".*?\]).*?/', $content,$matches);
					$european2017 = explode(',', $matches['galleryIds']);
					$content = preg_replace('/(\[et_pb_gallery gallery_ids=\"(?<galleryIds>.*?)\".*?module_id=\"2017european\".*?\]\[\/et_pb_gallery\]).*?/', '', $content);
					preg_match('/(\[et_pb_gallery gallery_ids=\"(?<galleryIds>.*?)\".*?module_id=\"22general\".*?\]).*?/', $content,$matches);
					$general22 = explode(',', $matches['galleryIds']);
					$content = preg_replace('/(\[et_pb_gallery gallery_ids=\"(?<galleryIds>.*?)\".*?module_id=\"22general\".*?\]\[\/et_pb_gallery\]).*?/', '', $content);

					$grid = '<div id="activities-container" class="grid">
					';

					
					foreach ($general24 as $galleryClass) {

						$thumb = wp_get_attachment_url( $galleryClass );
						$grid.= '
						<a style="margin-bottom:5px" class="et_pb_gallery_image activity-item general24" href="'.$thumb.'" >
							<div  >
								<img width="200" src="'.$thumb.'" alt="client_03" >
								<span class="et_overlay"></span>
								</div>
						</a>';
					}
					foreach ($european2018 as $galleryClass) {

						$thumb = wp_get_attachment_url( $galleryClass );
						$grid.= '
						<a style="margin-bottom:5px" class="et_pb_gallery_image activity-item european2018" href="'.$thumb.'" >
							<div  >
								<img width="200" src="'.$thumb.'" alt="client_03" style="margin-bottom: -1px;">
								<span class="et_overlay"></span>
								</div>
						</a>';
					}
					foreach ($general23 as $galleryClass) {

						$thumb = wp_get_attachment_url( $galleryClass );
						$grid.= '
						<a style="margin-bottom:5px" class="et_pb_gallery_image activity-item general23" href="'.$thumb.'" >
							<div  >
								<img width="200" src="'.$thumb.'" alt="client_03" style="margin-bottom: -1px;">
								<span class="et_overlay"></span>
								</div>
						</a>';
					}
					foreach ($european2017 as $galleryClass) {

						$thumb = wp_get_attachment_url( $galleryClass );
						$grid.= '
						<a style="margin-bottom:5px" class="et_pb_gallery_image activity-item european2017" href="'.$thumb.'" >
							<div  >
								<img width="200" src="'.$thumb.'" alt="client_03" style="margin-bottom: -1px;">
								<span class="et_overlay"></span>
							</div>
						</a>';
						
					}
					foreach ($general22 as $galleryClass) {

						$thumb = wp_get_attachment_url( $galleryClass );
						$grid.= '
						<a style="margin-bottom:5px" class="et_pb_gallery_image activity-item general22" href="'.$thumb.'" >
							<div >
								<img width="200" src="'.$thumb.'" alt="client_03" style="margin-bottom: -1px;">
								<span class="et_overlay"></span>
								</div>
						</a>';
					}


					$grid .= '</div>
					<div id="noResults"class="hidden visuallyhidden et_pb_module et_pb_text et_pb_text_5  et_pb_text_align_left et_pb_bg_layout_light et_had_animation">
						
						
						<div class="et_pb_text_inner"><h2>No se encontraron resultados</h2></div>
					</div>';

					$content = preg_replace('/(\[overtek_activities_filtered_list\])(.*?)(\[\/overtek_activities_filtered_list\])/', '[overtek_activities_filtered_list]'.$grid.'[/overtek_activities_filtered_list]', $content);
					echo apply_filters('the_content', $content);
					if ( ! $is_page_builder_used )
						wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</<?php echo $container_tag; ?>> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<?php

get_footer();
