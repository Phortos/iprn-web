<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
 
}

function divi_child_theme_setup() {
    if ( ! class_exists('ET_Builder_Module') ) {
        return;
    }
 
    get_template_part( 'custom-modules/customBlog' );
 
    $customBlog = new Custom_ET_Builder_Module_Blog();
 
    remove_shortcode( 'et_pb_blog' );
    
    add_shortcode( 'et_pb_blog', array($customBlog, '_shortcode_callback') );
    
}
 
add_action( 'wp', 'divi_child_theme_setup', 9999 );