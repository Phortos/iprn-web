<?php
// =============================================================================
// VIEWS/GLOBAL/_HEADER.PHP
// Declares the DOCTYPE for the site and includes the <head>.
// =============================================================================
?>
<!DOCTYPE html>
<!--[if IE 9]><html class="no-js ie9" <?php language_attributes(); ?>><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- 
         -+shmNMMMMNmhs/.             .::::::  ::::::::-`   -:::::::-.     ::::`      `:::::`       
     `:yNMMMMMMMMMMMMMMMMms-           .yMM-`  `.mMd.``-ymo``.mMm.``-sms`  `-NMN/      `-m-`        
   `+mMMNNdysssssydNNMMMMMMMd/          oMN      dMy     NMo  hMh     mMs    h:dMd-      h          
  -dMNy+/+shdmmmdhs+/+hNMMMMMMh.        oMN      dMy    -MN:  hMh    .NN/    h  +NMs.    h          
 /Nmo:smMMMMMMMMMMMMMms:omMMMMMN-       oMN      dMd//+oyo.   hMd//ohdo-     h   .yNN+`  h          
-Ny-yMMNmhyyyhdmNMMMMMMNs-yMMMMMm`      oMN      dMy          hMh  :mN+`     h     :dMd: h          
hs-mNy++oyhhhhso+odMMMMMMd.sMMMMMo      oMN      dMy          hMh   .yMh.    h      `+mMyd          
d`ds:smMMMMMMMMMNh//mMMMMMd`mMMMMd    ``yMM.`  `.mMd.`      `.dMd.`   /NN/``.m.`      .sNm          
+-+:mNdhyyhdNMMMMMMs.mMMMMM-oMMMMd    .:/++/-  -:+++:-      -:+++:-    .+/:-:+:-`       -o          
.``ds/oyhhys/+mMMMMM+:MMMMM-oMMMMo      .                       `             -  `...    -   `.`    
  .-+NMMMMMMNs.mMMMMy`MMMMN`hMMMm`      o :-:`o..--`:-.--.`-:`+.-`.--.---``-: +  :/-/-  -o---`/- --.
   `MMMMMMMMMM:+MMMM/:MMMM:/MMMN-       + + + +.+//-+ :- +:/+./.:.+.-/+ .://s +  :-` :--++-.+`//`+.-
    dMMMMMMMMm`yMMMo.mMMm:/NMMd.        ``` ` ` ``` `    ` `` ``   `` `   ``` `       ``````` .` ```
    `odMMMMms-sMNy:+NMmo:yMMd/`         +.:/ .. + ..`./`- ..` ..` ..  .s: + `..`/.` . ` .. `.`+``   
       `..`./so//sdho/+hMNy:            o-o`+-::+ :-o + +-: +`o +.:/  ././o s-/`+ /:+:/+``+/. s+    
             `-///+syhs/.               -  -`--`. --- -`. --- . .`--  `.  : .-- -. - : `--`.  .`. 
-->
<head>
  <?php wp_head(); ?>
    
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-136545551-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
 
  gtag('config', 'UA-136545551-1');
</script>
    
</head>

<body <?php body_class(); ?>>
    
  <?php do_action( 'x_before_site_begin' ); ?>

  <div id="top" class="site">

  <?php do_action( 'x_after_site_begin' ); ?>