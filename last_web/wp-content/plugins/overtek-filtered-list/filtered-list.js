   
var itemSelector = ".member-item"; 
var $checkboxes = jQuery('.member-filter');
var $container = jQuery('#members-container').isotope({ itemSelector: itemSelector, masonry: { gutter:30 } });

//Ascending order
var responsiveIsotope = [ [480, 8] , [800, 8] ];
var itemsPerPageDefault = 9;
var itemsPerPage = defineItemsPerPage();
var currentNumberPages = 1;
var currentPage = 1;
var currentFilter = '*';
var filterAttribute = 'data-filter';
var filterValue = "";
var pageAttribute = 'data-page';
var pagerClass = 'isotope-pager';

// update items based on current filters    
function changeFilter(selector) { $container.isotope({ filter: function() {
  var name = jQuery(this).find('.tituloAgencia').text();
  var textoInput = jQuery('#inputBusqueda').val();
  if (textoInput.length >= 3) {
    return jQuery(this).is(selector) &&  name.match( new RegExp(textoInput,'i') )
  } else {
    return jQuery(this).is(selector)
  }
  
} }) }



//grab all checked filters and goto page on fresh isotope output
function goToPage(n) {
    currentPage = n;
    var selector = itemSelector;
    var exclusives = [];
    filtroCategoria = jQuery('#combobox').val() ? jQuery('#combobox').val().join('') : '';
    filtroPais = jQuery('#selectBoxPaises').val() ? jQuery('#selectBoxPaises').val() : '';
    filterValue = filtroPais === '' && filtroCategoria === '' ? '*' : filtroCategoria+filtroPais;
    // add page number to the string of filters
    var wordPage = currentPage.toString();
    filterValue += ('.'+wordPage);
   
    changeFilter(filterValue);
    if (jQuery('#members-container').isotope('getFilteredItemElements').length == 0) {
      _mostrarNoResultados(true);
    } else {
      _mostrarNoResultados(false);
    }
}

// determine page breaks based on window width and preset values
function defineItemsPerPage() {
    var pages = itemsPerPageDefault;

    for( var i = 0; i < responsiveIsotope.length; i++ ) {
        if( jQuery(window).width() <= responsiveIsotope[i][0] ) {
            pages = responsiveIsotope[i][1];
            break;
        }
    }
    return pages;
}

function _desaparecerContainer (mostrar, borrar, id) {
  // ANIMAR CONTENEDOR
  
  var contenedor = jQuery('#'+id);
  if (mostrar) {
    contenedor.removeClass('hidden');
    setTimeout(function () {
      contenedor.removeClass('visuallyhidden');
    }, 20);
  } else {
    if (!contenedor.hasClass('visuallyhidden')) {
      contenedor.addClass('visuallyhidden');
      var myF = function(e) {
        if (borrar != null) {
          borrar.remove();
        }
        contenedor.addClass('hidden');
        contenedor.off('transitionend', myF)
      }
      contenedor.on('transitionend', myF);
    } else {
      contenedor.addClass('hidden');
    }
  }
}

function _mostrarNoResultados (mostrar) {
  if (mostrar) {
    jQuery('#noResults').removeClass('hidden');
  } else {
    if (!jQuery('#noResults').hasClass('hidden')) {
      jQuery('#noResults').addClass('hidden');
    }
  }
  
  
}

function setPagination() {

    var SettingsPagesOnItems = function(){
        var itemsLength = $container.children(itemSelector).length;
        var pages = Math.ceil(itemsLength / itemsPerPage);
        var item = 1;
        var page = 1;
        var selector = itemSelector;
        var exclusives = [];
        filtroCategoria = jQuery('#combobox').val() ? jQuery('#combobox').val().join('') : '';
        filtroPais = jQuery('#selectBoxPaises').val() ? jQuery('#selectBoxPaises').val() : '';
        filterValue = filtroPais === '' && filtroCategoria === '' ? '*' : filtroCategoria+filtroPais;
        // find each child element with current filter values
        $container.children(filterValue).each(function(){
          var name = jQuery(this).find('.tituloAgencia').text();
          var textoInput = jQuery('#inputBusqueda').val();
          if (textoInput.length < 3 || (textoInput.length >= 3 && name.match( new RegExp(textoInput,'i')))) {
          // increment page if a new one is needed
            if( item > itemsPerPage ) {
                page++;
                item = 1;
                
            }
            // add page number to element as a class
            wordPage = page.toString();
            var classes = jQuery(this).attr('class').split(' ');
            var lastClass = classes[classes.length-1];
            // last class shorter than 4 will be a page number, if so, grab and replace
            if(lastClass.length < 4){
              jQuery(this).removeClass();
                classes.pop();
                classes.push(wordPage);
                classes = classes.join(' ');
                jQuery(this).addClass(classes);
            } else {
                // if there was no page number, add it
                jQuery(this).addClass(wordPage); 
            }
            item++;
          }
        });
        currentNumberPages = page;
    }();

    // create page number navigation
    var CreatePagers = function() {

        var $isotopePager = ( jQuery('.'+pagerClass).length == 0 ) ? jQuery('<div class="'+pagerClass+'"></div>') : jQuery('.'+pagerClass);

        $isotopePager.html('');
        if(currentNumberPages > 1){
          for( var i = 0; i < currentNumberPages; i++ ) {
              var activo = i == 0 ? ' activo' : ''
              var $pager = jQuery('<a href="javascript:void(0);" class="hoverJose pager'+activo+'" '+pageAttribute+'="'+(i+1)+'"></a>');
                  $pager.html(i+1);
                  $pager.click(function(){
                    jQuery('.pager').removeClass('activo');
                    jQuery(this).addClass('activo');
                    var page = jQuery(this).eq(0).attr(pageAttribute);
                    goToPage(page);
                });
            $pager.appendTo($isotopePager);
        }
      }
      $container.after($isotopePager);
  }();
}
setPagination();
for(var i = 0; i < defineItemsPerPage(); i++) {
  goToPage(i);
}
goToPage(1);



jQuery(window).resize(function(){
  itemsPerPage = defineItemsPerPage();
  setPagination();
  goToPage(1);
});


jQuery( function() {


  //MIEMBROS

  jQuery.widget( "custom.combobox", {
    _create: function() {
      this.wrapper = jQuery(`<div id="filtros-seleccionados" style="
      
      display: inline-block;
      
      
      width:300px;
      box-sizing: border-box;">
        <div id="contenedor" class="hidden visuallyhidden et_pb_module et_pb_filterable_portfolio et_pb_filterable_portfolio_0 et_pb_portfolio  et_pb_bg_layout_light et_pb_filterable_portfolio_grid clearfix" data-posts-number="10" style="margin:0;height:100%;border-top: 1px solid #ccc;
        padding: 6px 2px 4px 2px;
        transition: visibility 0s, opacity 0.5s linear;
        width: 100%;">
          <div class="et_pb_portfolio_filters clearfix" style="margin:0;height:100%">
            <ul class="clearfix" style="height:100%">
            </ul>
          </div>
        </div>
      </div>`)
        .addClass( "custom-combobox" )
        .attr("placeholder", "Your Text Here")
        .insertAfter( this.element );
      this.element.hide();
      this._createAutocomplete();
      this._createShowAllButton();
      this._createClearAllButton();
    },

    _createAutocomplete: function() {
      var selected = this.element.children( ":selected" ),
        value = selected.val() ? selected.text() : "";
      
      this.input = jQuery( '<input style="height: 41px;;padding-right: 0;cursor: text;border:none;background: transparent;width:75%;margin-bottom:6px;border-bottom: 2px solid #00a19a;border-radius: 4px 4px 0 0;" >' )
        .prependTo( '#filtros-seleccionados' )
        .val( value )
        .attr( "title", "" )
        .attr( "placeholder", "Choose sectors" )
        .addClass( "custom-combobox-input  ui-widget-content ui-state-default ui-corner-left" )
        .autocomplete({
          delay: 0,
          minLength: 0,
          source: jQuery.proxy( this, "_source" ),
          select: jQuery.proxy( this, "_change" )
        })
        .tooltip({
          classes: {
            "ui-tooltip": "ui-state-highlight"
          }
        });

      this._on( this.input, {
        autocompleteselect: function( event, ui ) {
          var mostrar = this.value == null;
          ui.item.option.selected = true;
          var filter = ui.item.value;
          currentFilter = filter;
          setPagination();
          if (mostrar) {
            _desaparecerContainer(mostrar, null, 'contenedor');
          }
          jQuery('#trashButton').button( "option", "disabled", false );
          goToPage(1);
          var nuevoFiltro = jQuery(`<li class="et_pb_portfolio_filter" style="padding:4px 6px; width:auto;" >
          <a style="padding: 5px 5px; border-radius:3px; margin-left:10px" class="active" ><span>`+ui.item.label+`</span><span class="removeItem" style="cursor:pointer">   x</span></a>
          </li>`).on('click', '.removeItem', function() {
            var mostrar = jQuery('#combobox').val() != null && jQuery('#combobox').val().length != 1
            var cat = jQuery('#combobox').val().filter(x => x != ui.item.value)
            ui.item.option.selected = false
            // jQuery('#combobox').val('');
            // jQuery('#combobox').val(cat);
            
            if (mostrar) {
              jQuery(this).closest('li').remove()
            } else {
              _desaparecerContainer(false, jQuery(this).closest('li'),'contenedor');
              jQuery('#trashButton').button( "option", "disabled", true);
            }
            
            setPagination();
            goToPage(1);
          });
          jQuery('#filtros-seleccionados ul').append(nuevoFiltro);
          // this.input.append(nuevoFiltro);
          this._trigger( "select", event, {
            item: ui.item.option
          });
          jQuery(this).val('');
          return false;
        },
        autocompletechange: "_removeIfInvalid"
      });
    },

    _createShowAllButton: function() {
      var input = this.input,
        wasOpen = false;

      jQuery( '<a style="float:right;border-radius: 34px;height: 32px;width: 32px;background: none;margin-top: 5px;border: 1px solid #00a19a;">' )
        .attr( "tabIndex", -1 )
        .attr( "title", "Show All Items" )
        .tooltip()
        .appendTo('#filtros-seleccionados')
        .addClass( "custom-combobox-toggle" )
        .button({
          icons: {
            primary: "ui-icon-triangle-1-s"
          },
          text: false
        })
        .on( "mousedown", function() {

          wasOpen = input.autocomplete( "widget" ).is( ":visible" );
        })
        .on( "click", function() {
          input.trigger( "focus" );
          // Close if already visible
          if ( wasOpen ) {
            return;
          }

          // Pass empty string as value to search for, displaying all results
          input.autocomplete( "search", "" );
        });
    },
    _createClearAllButton: function() {
      var input = this.input,
        wasOpen = false;

      jQuery( '<a id="trashButton" style="float:right;border-radius: 34px;height: 32px;width: 32px;background: none;margin-top: 5px;border: 1px solid #00a19a;">' )
        .attr( "tabIndex", -1 )
        .attr( "title", "Delete All Filters" )
        .tooltip()
        .prop("disabled", true)
        .appendTo('#filtros-seleccionados')
        .removeClass( "ui-corner-all" )
        .addClass( "custom-combobox-trash ui-corner-top-right" )
        .button({
          icons: {
            primary: "ui-icon-trash"
          },
          text: false
        })
        .on( "click", function() {
          jQuery('#combobox').val([])
          currentFilter = '*';
          this.value = [];
          jQuery('#trashButton').button( "option", "disabled", true);
          _desaparecerContainer(false, jQuery( '#filtros-seleccionados li.et_pb_portfolio_filter' ),'contenedor');
          setPagination();
          goToPage(1);
        })
    },

    

    _source: function( request, response ) {
      var matcher = new RegExp( jQuery.ui.autocomplete.escapeRegex(request.term), "i" );
      response( this.element.children( "option" ).filter(function () {return this.index != 0 &&  !this.selected}).map(function() {
        var text = jQuery( this ).text();
        if ( this.value && ( !request.term || matcher.test(text) ) )
          return {
            label: text,
            value: this.value,
            option: this
          };
      }) );
    },
    _change: function( request, response ) {
      this.input.val('');
      return false;
    },


    _removeIfInvalid: function( event, ui ) {

      // Selected an item, nothing to do
      if ( ui.item ) {
        jQuery(this).val('');
        return false;
      }

      // Search for a match (case-insensitive)
      var value = this.input.val(),
        valueLowerCase = value.toLowerCase(),
        valid = false;
      this.element.children( "option" ).each(function() {
        
        if ( jQuery( this ).text().toLowerCase() === valueLowerCase ) {
          this.selected = valid = true;
          return false;
        }
      });

      // Found a match, nothing to do
      if ( valid ) {
        jQuery(this).val('');
        return false;
      }

      // Remove invalid value
      this.input
        .val( "" )
        .attr( "title", value + " didn't match any item" )
        .tooltip( "open" );
      this.element.val( "" );
      this._delay(function() {
        this.input.tooltip( "close" ).attr( "title", "" );
      }, 2500 );
      this.input.autocomplete( "instance" ).term = "";
    },

    _destroy: function() {
      this.wrapper.remove();
      this.element.show();
    }
  });

  // PAISES

  jQuery.widget( "custom.selectBox", {
    _create: function() {
      this.wrapper = jQuery(`<div id="filtro-`+this.element[0].id+`" style="   
      display: inline-block;
      height: 41px;
      width:300px;
      box-sizing: border-box;">
      </div>`)
        .addClass( "custom-combobox" )
        .attr("placeholder", "Your Text Here")
        .insertAfter( this.element );
      this.element.hide();
      this._createAutocomplete();
      this._createShowAllButton();
      this._createClearAllButton();
    },

    _createAutocomplete: function() {
      var selected = this.element.children( ":selected" ),
        value = selected.val() ? selected.text() : "";
      this.input = jQuery( '<input style="padding-right: 0;cursor: text;border:none;background: transparent;height:100%;width:75%;margin-bottom:6px;border-bottom: 2px solid #00a19a;border-radius: 4px 4px 0 0;" >' )
        .prependTo( '#filtro-'+this.element[0].id )
        .val( value )
        .attr( "title", "" )
        .attr( "placeholder", this.options.placeholder )
        .addClass( "custom-combobox-input  ui-widget-content ui-state-default ui-corner-left" )
        .autocomplete({
          delay: 0,
          minLength: 0,
          source: jQuery.proxy( this, "_source" )
        })
        .tooltip({
          classes: {
            "ui-tooltip": "ui-state-highlight"
          }
        });

      this._on( this.input, {
        autocompleteselect: function( event, ui ) {
          ui.item.option.selected = true;
          var filter = ui.item.value;
          currentFilter = filter;
          setPagination();
          jQuery('#trash-'+this.element[0].id).button( "option", "disabled", false );
          goToPage(1);
          // this.input.append(nuevoFiltro);
          this._trigger( "select", event, {
            item: ui.item.option
          });
          this.input.val(ui.item.label)
          return false;
        },
        autocompletechange: "_removeIfInvalid"
      });
    },

    _createShowAllButton: function() {
      var input = this.input,
        wasOpen = false;

        jQuery( '<a style="float:right;border-radius: 34px;height: 32px;width: 32px;background: none;margin-top: 5px;border: 1px solid #00a19a;">' )
        .attr( "tabIndex", -1 )
        .attr( "title", "Show All Items" )
        .tooltip()
        .appendTo('#filtro-'+this.element[0].id )
        .button({
          icons: {
            primary: "ui-icon-triangle-1-s"
          },
          text: false
        })
        .addClass( "custom-combobox-toggle" )
        .on( "mousedown", function() {

          wasOpen = input.autocomplete( "widget" ).is( ":visible" );
        })
        .on( "click", function() {
          input.trigger( "focus" );
          // Close if already visible
          if ( wasOpen ) {
            return;
          }

          // Pass empty string as value to search for, displaying all results
          input.autocomplete( "search", "" );
        });
    },
    _createClearAllButton: function() {
      var idElement = this.element[0].id
      var input = this.input,
      
        wasOpen = false;

      jQuery( '<a id="trash-'+idElement+'" style="margin-left:10px;float:right;border-radius: 34px;height: 32px;width: 32px;background: none;margin-top: 5px;border: 1px solid #00a19a;">' )
        .attr( "tabIndex", -1 )
        .attr( "title", "Delete All Filters" )
        .tooltip()
        .prop("disabled", true)
        .appendTo('#filtro-'+this.element[0].id )
        .button({
          icons: {
            primary: "ui-icon-trash"
          },
          text: false
        })
        .removeClass( "ui-corner-all" )
        .addClass( "custom-combobox-trash ui-corner-top-right" )
        .on( "click", function() {
          jQuery('#'+idElement).val("")
          currentFilter = '*';
          input.val('');
          this.value = "";
          jQuery('#trash-'+idElement).button( "option", "disabled", true);
          setPagination();
          goToPage(1);
        })
    },

    

    _source: function( request, response ) {
      var matcher = new RegExp( jQuery.ui.autocomplete.escapeRegex(request.term), "i" );
      response( this.element.children( "option" ).filter(function () {return this.index != 0 &&  !this.selected}).map(function() {
        var text = jQuery( this ).text();
        if ( this.value && ( !request.term || matcher.test(text) ) )
          return {
            label: text,
            value: this.value,
            option: this
          };
      }) );
    },


    _removeIfInvalid: function( event, ui ) {

      // Selected an item, nothing to do
      if ( ui.item ) {
        return false;
      }

      // Search for a match (case-insensitive)
      var value = this.input.val(),
        valueLowerCase = value.toLowerCase(),
        valid = false;
      this.element.children( "option" ).each(function() {
        
        if ( jQuery( this ).text().toLowerCase() === valueLowerCase ) {
          this.selected = valid = true;
          return false;
        }
      });

      // Found a match, nothing to do
      if ( valid ) {
        return false;
      }

      // Remove invalid value
      this.input
        .val( "" )
        .attr( "title", value + " didn't match any item" )
        .tooltip( "open" );
      this.element.val( "" );
      this._delay(function() {
        this.input.tooltip( "close" ).attr( "title", "" );
      }, 2500 );
      this.input.autocomplete( "instance" ).term = "";
    },

    _destroy: function() {
      this.wrapper.remove();
      this.element.show();
    }
  });
  jQuery('#inputBusqueda').on('input', function() {
    // do something
    setPagination();
    goToPage(1);
  });
  console.log(jQuery(window).width());
  // PC PEQUEÑO
  if (jQuery(window).width() < 1300) {
    jQuery( ".member-item" ).css({'width': '280px','margin': '10px 0px'});
    jQuery( "#members-container" ).css({'margin-left': '50px'});
  }

  // IPAD TUMBADO
  if (jQuery(window).width() < 1115) {
    jQuery( ".member-item" ).css({'width': '250px','margin': '20px 10px'});
    // jQuery( ".isotope-pager a").css({'margin': '10px', 'text-align': 'center','width': '42,1px'});
    // jQuery( ".tituloAgencia").css({'font-weight':'bold', 'height':'50px','min-height':'50px'});
    jQuery( "#members-container" ).css({'margin-left': '0px'});
    
  }
  // IPAD
  if (jQuery(window).width() < 800) {
    jQuery( ".member-item" ).css({'width': '250px','margin': '20px 10px'});
    jQuery( ".isotope-pager a").css({'margin': '10px', 'text-align': 'center','width': '42,1px'});
    jQuery( ".tituloAgencia").css({'font-weight':'bold', 'height':'50px','min-height':'50px'});
    jQuery( "#members-container" ).css({'margin-left': '20px'});
    
  }
  // MOVIL
  if (jQuery(window).width() < 770) {
    jQuery( ".member-item" ).css({'width': '250px','margin': '20px 10px'});
    jQuery( ".isotope-pager a").css({'margin': '10px', 'text-align': 'center','width': '42,1px'});
    jQuery( "#members-container" ).css({'margin-left': '20px'});
    jQuery( ".inputAgencias").css({'width':'300px'});
    jQuery( ".inputAgencias input").css({'width':'384px'});
  }
  // MOVIL
  if (jQuery(window).width() < 500) {
    jQuery( ".member-item" ).css({'width': '280px','margin': '20px 10px'});
    jQuery( ".isotope-pager a").css({'margin': '10px', 'text-align': 'center','width': '42,1px'});
    jQuery( "#members-container" ).css({'margin-left': '20px'});
    jQuery( ".inputAgencias").css({'width':'300px'});
    jQuery( ".inputAgencias input").css({'width':'384px'});
  }
   // MOVIL
   if (jQuery(window).width() < 400) {
    jQuery( ".member-item" ).css({'width': '260px','margin': '20px 10px'});
    jQuery( ".isotope-pager a").css({'margin': '10px', 'text-align': 'center','width': '42,1px'});
    jQuery( "#members-container" ).css({'margin-left': '5px'});
    jQuery( ".inputAgencias").css({'width':'300px'});
    jQuery( ".inputAgencias input").css({'width':'384px'});
  }
  jQuery( "#combobox" ).combobox();
  jQuery( "#selectBoxPaises" ).selectBox({placeholder: 'Choose a country'});
  jQuery( "#toggle" ).on( "click", function() {
    jQuery( "#combobox" ).toggle();
  });
} );







