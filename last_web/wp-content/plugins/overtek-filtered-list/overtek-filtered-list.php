<?php
/**
* Plugin Name: Overtek FILTERED LIST
* Plugin URI: https://www.overtek.es/
* Description: 
* Version: 1.0
* Author: Jose Bayona
* Author URI: http://www.overtek.es/
**/

require_once __DIR__.'/vendor/autoload.php';

use Doctrine\Common\Collections\ArrayCollection;

function overtek_filtered_list_init()
{


  function filter_invalid_term_ids( $term_ids, $taxonomy ) {
		$valid_term_ids = array();

		foreach ( $term_ids as $term_id ) {
			$term_id = intval( $term_id );
			$term = term_exists( $term_id, $taxonomy );
			if ( ! empty( $term ) ) {
				$valid_term_ids[] = $term_id;
			}
		}

		return $valid_term_ids;
	}


  function ofl_get_members ($args) {
    $query_args = array();
    $include_categories = filter_invalid_term_ids( explode( ',', $args['include_categories'] ), 'project_category' );
    // $include_categories = $args['include_categories'];

		if ( ! empty( $include_categories ) ) {
			$query_args['tax_query'] = array(
				array(
					'taxonomy' => 'project_category',
					'field'    => 'id',
					'terms'    => $include_categories,
          'operator' => 'IN'
          
				)
			);
		}

		$default_query_args = array(
			'post_type'   => 'project',
			'post_status' => 'publish',
      'posts_per_page' => -1,
      'orderby'=> 'title', 'order' => 'ASC'
		);
		$query_args = wp_parse_args( $query_args, $default_query_args );

		// Get portfolio query
		$query = new WP_Query( $query_args );

		
		
    
    return $query;
  }

  function ofl_get_projects ($args) {
    $query_args = array();
    $include_categories = filter_invalid_term_ids( explode( ',', $args['include_categories'] ), 'project_category' );
    // $include_categories = $args['include_categories'];

		if ( ! empty( $include_categories ) ) {
			$query_args['tax_query'] = array(
				array(
					'taxonomy' => 'project_category',
					'field'    => 'id',
					'terms'    => $include_categories,
          'operator' => 'IN'
          
				)
			);
		}
		$default_query_args = array(
			'post_type'   => 'project',
			'post_status' => 'publish',
      'posts_per_page' => -1
		);
		$query_args = wp_parse_args( $query_args, $default_query_args );

		// Get portfolio query
		$query = new WP_Query( $query_args );
    return $query;
  }

  function inicializar () {
    wp_enqueue_script('isotope', '/wp-content/plugins/overtek-filtered-list/isotope.pkgd.min.js',[], null);
    wp_enqueue_script('cells_by_row_js', '/wp-content/plugins/overtek-filtered-list/cells-by-row.js', ['isotope'],null);
    
    
    wp_enqueue_script('jquery');
    // wp_enqueue_script('jquery-ui', '/wp-includes/js/jquery/jquery-ui.min.js');
    wp_enqueue_style('jose-css', "https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css");
    wp_enqueue_script( 'jquery-ui-core' );
    wp_enqueue_script( 'jquery-ui-widget' );
    wp_enqueue_script( 'jquery-ui-position' );
    wp_enqueue_script( 'jquery-ui-menu' );
    wp_enqueue_script( 'jquery-ui-autocomplete' );
    wp_enqueue_script( 'jquery-ui-button' );
    wp_enqueue_script( 'jquery-ui-tooltip' );
    wp_enqueue_script( 'jquery-ui-toggle' );
    


    
  }

  function get_members_specialisms ($query) {
    $retorno = new ArrayCollection([]);
    if( $query->have_posts() ) {
      while ( $query->have_posts() ) {
        $query->the_post();
        um_fetch_user(get_post_meta(get_the_ID(),'member_id', true));
        $specialisms = um_user('Specialisms_36');
        if ($specialisms) {
          foreach ($specialisms as $specialism ) {
            if (!$retorno->exists(function($key, $value) use ($specialism) {
              return $value['especialidad'] === $specialism;
            })) {
              $retorno->add( [
                "especialidad" => $specialism,
                "clave" => preg_replace("/[^\w]/",'',$specialism)
              ]);
            }
          }
        }
      }
    }
    return $retorno->toArray();
  }


  function get_members_countries ($query) {
    $retorno = new ArrayCollection([]);
    if( $query->have_posts() ) {
      while ( $query->have_posts() ) {
        $query->the_post();
        um_fetch_user(get_post_meta(get_the_ID(),'member_id', true));
        $pais = um_user('country');
        if ($pais) {
          if (!$retorno->exists(function($key, $value) use ($pais) {
            return $value['pais'] === $pais;
          })) {
            $retorno->add( [
              "pais" => $pais,
              "clave" => preg_replace("/[^\w]/",'',$pais)
            ]);
          }
        }
      }
    }
    return $retorno->toArray();
  }
  function get_projects_years () {
    $categorias = get_terms('project_category');
    if ($categorias) {
      $categorias = array_map(function ($cat) {
        return [
          'texto' => $cat->name,
          'id' => $cat->term_id,
          'clave' => preg_replace("/[^\w]/",'',$cat->slug)];
      }, $categorias);
    }
    $categorias = array_filter($categorias, function ($cat) {
      return intval($cat['id']) > 68 && $cat['texto'] != 'Ganador';
    });
    return $categorias;
  }

  function get_members_names ($query) {
    $retorno = new ArrayCollection([]);
    if( $query->have_posts() ) {
      while ( $query->have_posts() ) {
        $query->the_post();
        um_fetch_user(get_post_meta(get_the_ID(),'member_id', true));
        $nombre = um_user('nickname');
        if ($nombre) {
          if (!$retorno->exists(function($key, $value) use ($nombre) {
            return $value['nombre'] === $nombre;
          })) {
            $retorno->add( [
              "nombre" => $nombre,
              "clave" => preg_replace("/[^\w]/",'',$nombre)
            ]);
          }
        }
      }
    }
    return $retorno->toArray();
  }


  function multiSelectCategorias ($query) {
    $specialisms = get_members_specialisms($query);
    $retorno = '';
     $retorno .= '<div class="inputJose  multiselectJose" style="margin: 8px 0"><select id="combobox" multiple>';
    foreach ($specialisms as $specialism ) {
      $retorno .='<option value=".'.$specialism['clave'].'">'.$specialism['especialidad'].'</option>';
    }
    $retorno .='</select></div>';
    return $retorno;
  }

  function selectPaises ($query) {
    $countries = get_members_countries(($query));
    $retorno = '';
     $retorno .= '<div class="inputJose"  style=";margin:8px 0;height:fit-content" ><select id="selectBoxPaises">
     <option value="">Select one...</option>';
    foreach ($countries as $country ) {
      $retorno .='<option value=".'.$country['clave'].'">'.$country['pais'].'</option>';
    }
    $retorno .='</select></div>';
    return $retorno;
  }


  function selectAnios () {
    $anios = get_projects_years();
    $retorno = '';
     $retorno .= '<div class="inputJose"  style="margin:8px 0;height:fit-content;" ><select id="selectBoxAnios">
     <option value="">Select one...</option>';
    foreach ($anios as $anio ) {
      $retorno .='<option value=".'.$anio['clave'].'">'.$anio['texto'].'</option>';
    }
    $retorno .='</select></div>';
    return $retorno;
  }

  function selectAgencias ($query) {
    $agencias = get_members_names(($query));
    $retorno = '<div class=" inputAgencias inputJose" style="
    margin:8px 0;
    
    border-bottom: 2px solid #00a19a;
    border-radius: 4px 4px 0 0;
    height:41.667px;
    width:215px;
    box-sizing: border-box;">
    <input id="inputBusqueda" placeholder="Search an agency" style="cursor: text;border:none;background: none;height:100%;width:100%;margin-bottom:6px; margin-left:16px" >
    </div>';
    return $retorno;
  }

  function buttonsProjects () {
    $retorno = '';
    $categorias = get_terms('project_category');
    if ($categorias) {
      $categorias = array_map(function ($cat) {
        return [
          'texto' => $cat->name,
          'idBBDD' => $cat->term_id,
          'id' => preg_replace("/[^\w]/",'',$cat->slug)];
      }, $categorias);
    }
    $categorias = array_filter($categorias, function ($cat) {
      return intval($cat['idBBDD']) <= 65;
    });
    array_unshift($categorias,[
      'texto' => 'ALL',
      'id' => 'all'
    ]);
    foreach ($categorias as $category) {
      $id = $category['id'];
      $texto = $category['texto'];
      $activo = $category['id'] == 'all' ? ' activo' : '';
      $retorno .= '<div class="hoverJose categoryButton'.$activo.'" id="category-'.$id.'" style="
      padding: 8px 0px 0px 0;
      width:auto;
      text-align: center;
      min-width: 75px;
      font-size: 1em;
      margin:8px 0;
      border-radius: 4px;
      border-color: #00a19a;
      
      cursor:pointer">
    <span style="
    height:100%;
    margin: 10px;
    
    " >'.$texto.'</span>
    </div>';
    }
    
    return $retorno;
  }
  function buttonsActivities () {
    $retorno = '';
    $categorias = [
      ["id" => 'general24', "texto" => '24TH ANNUAL GENERAL MEETING'], ["id" => 'european2018', "texto" => 'EUROPEAN MEETING 2018'], ["id" => 'general23', "texto" => '23RD ANNUAL GENERAL MEETING'], ["id" => 'european2017', "texto" => 'EUROPEAN REGIONAL MEETING 2017'], ["id" => 'general22', "texto" => '22ND ANNUAL GENERAL MEETING']
    ];
    foreach ($categorias as $category) {
      $id = $category['id'];
      $texto = $category['texto'];
      $activo = $category['id'] == 'general24' ? ' activo' : '';
      $retorno .= '<div class="hoverJose categoryButton'.$activo.'" id="category-'.$id.'" style="
      padding: 8px 0px 8px 0;
      width:auto;
      text-align: center;
      min-width: 75px;
      font-size: 1em;
      margin:10px;
      border-radius: 4px;
      border-color: #00a19a;
      cursor:pointer">
    <span style="
    height:100%;
    margin: 10px;
    
    " >'.$texto.'</span>
    </div>';
    }
    
    return $retorno;
  }

  function pintarSeccionMembers() {
    $retorno = '';
    $seccionesGenerales = ['Documents','Committee', 'AI center', 'Minutes','Resources'];
    for ($i = 0; $i < count($seccionesGenerales); $i++) {
      $clases = '';
      $estilos = '';
      switch ($i) {
        case 0:
          $estilos = 'border-radius: 4px 0 0 0px;';
          $clases = 'activo';
        break;
        case 4:
          $estilos = 'border-radius: 0px 4px 0px 0px;';
          $clases = '';
        break;
        default:
          $estilos = 'border-radius: 0px;border-right:none;';
          $clases = '';
        
      }
      $retorno .= '<div id="'.$seccionesGenerales[$i].'" class="top categoryButton '.$clases.'"  style="'.$estilos.'
      padding: 8px 0px 8px 0;
      
      text-align: center;
      min-width: 75px;
      font-size: 1em;
      
      border-color: #00a19a;
      cursor:pointer">
    <span style="
    height:100%;
    margin: 10px;
    
    " >'.$seccionesGenerales[$i].'</span>
    </div>';
    }
    return $retorno;
  }


  function overtek_member_area_shortcode($atts = [], $content = null, $tag = '') {
    wp_enqueue_script('member_area_js', '/wp-content/plugins/overtek-filtered-list/member-area.js',[],null);
    wp_enqueue_style('fontawesome', '/wp-content/plugins/overtek-filtered-list/css/all.css',[],null);
    $aux = '<div style="display:flex;flex-wrap:wrap;justify-content_space-around">';
    $aux .= pintarSeccionMembers().'</div>';
    $aux .= '<div id="contenedorTab">
    <div style="display:flex;width:100%;flex-wrap:wrap">
      <div class="cabeceraGeneral" ></div>
      <div style="width:100%;display:flex">
        <div class="seccionIzquierda" ></div>
        <div style="width:70%;min-height:800px;height:800px; position:relative" class="contenedorDocumento">
          <div id="noDoc" style="width: 100%;text-align: center;padding-top:30px">Choose one document to see its preview or donwload it</div>
          <div id="siDoc" class="desactivado">
          <iframe class="iframe activo" id="iFrameDoc"  src="" title="Embedded Document" style="width:100%; height:100%; border: none; position: absolute;left:0;top:0;"></iframe>
          <img  style="width:100%" class="logo" src=""  width="100%">
          <a id="downloadButton" href="" class="hoverJose categoryButton download" download  style="
              padding: 8px;
              position:absolute;
              right: 20px;
              top:20px;
              width:40px;
              text-align: center;
              font-size: 1em;
              border-radius: 30px;
              border-color: #00a19a;
              cursor:pointer">
              <i style="width: 20px;height: 20px;" class="fas fa-download"></i>
            </a>
            <a id="cerrarButton"  class="hoverJose categoryButton"  style="
              padding: 8px;
              position:absolute;
              left: 20px;
              top:20px;
              display:none;
              width:40px;
              text-align: center;
              font-size: 1em;
              border-radius: 30px;
              border-color: #00a19a;
              cursor:pointer">
              <i style="width: 20px;height: 20px;" class="fas fa-window-close"></i>
            </a>
        </div>
        </div>
      </div>
    </div></div>';
    return $aux;
  }

  function overtek_activities_filtered_list_shortcode($atts = [], $content = null, $tag = '') {
    inicializar();
    wp_enqueue_script('filtered_activities_js', '/wp-content/plugins/overtek-filtered-list/filtered-list-activities.js', ['isotope'],null);
    $fullwidth = 'on';
    $width     = 'on' === $fullwidth ?  1080 : 400;
    $width     = (int) apply_filters( 'et_pb_portfolio_image_width', $width );
    $height    = 'on' === $fullwidth ?  9999 : 284;
    $height    = (int) apply_filters( 'et_pb_portfolio_image_height', $height );
    $classtext = 'on' === $fullwidth ? 'et_pb_post_main_image' : '';
    $aux = '<div style="justify-content:center;display:flex;flex-wrap:wrap; margin-bottom: 50px">';
    $aux .= buttonsActivities().'</div>';
    // $aux .= selectAnios();
    $aux .= $content;
   
    return $aux;
  }


  function overtek_projects_filtered_list_shortcode($atts = [], $content = null, $tag = '') {
    inicializar();
    wp_enqueue_script('filtered_projects_js', '/wp-content/plugins/overtek-filtered-list/filtered-list-projects.js', ['isotope', 'cells_by_row_js'],null);
    $show_pagination = 'on';
    $posts_number = '-1';
    $include_categories = get_terms('project_category');
    if ($include_categories) {
      $include_categories = array_map(function ($cat) {
        
        return $cat->term_id;
      }, $include_categories);
    }
   
    $include_categories = array_filter($include_categories, function ($cat) {
      return $cat != 68;
    });
    $include_categories =  join(",",$include_categories);
    $fullwidth = 'on';
    $query = ofl_get_projects( array(
      'show_pagination'    => $show_pagination,
      'include_categories' => $include_categories,
      'posts_number'       => $posts_number,
      'fullwidth'          => $fullwidth
    ));
    $width     = 'on' === $fullwidth ?  1080 : 400;
    $width     = (int) apply_filters( 'et_pb_portfolio_image_width', $width );
    $height    = 'on' === $fullwidth ?  9999 : 284;
    $height    = (int) apply_filters( 'et_pb_portfolio_image_height', $height );
    $classtext = 'on' === $fullwidth ? 'et_pb_post_main_image' : '';
    $aux = '<div style="justify-content:space-around;display:flex;flex-wrap:wrap; margin-bottom: 30px">';
    $aux .= buttonsProjects();
    $aux .= selectAnios().'</div>';
    $aux .= '<div id="projects-container" class="grid" style="margin-left:100px">';
    if( $query->have_posts() ) {
      while ( $query->have_posts() ) {
        $query->the_post();
        $id = get_the_ID();
        $titulo = get_the_title();
        $client = get_post_meta(get_the_ID(),'project_client', true);
        $agency = get_post_meta(get_the_ID(),'project_agency', true);
        $categorias = wp_get_post_terms( get_the_ID(), 'project_category');
        $ganador = false;
        // $alttext   = get_the_terms(get_the_ID(),'project_category');
        if ($categorias) {

          $categorias = array_map(function ($cat) {
            return preg_replace("/[^\w]/",'',$cat->slug);
          }, $categorias);
          $collCat = new ArrayCollection($categorias);
          $ganador = in_array('ganador',$categorias);
          $categorias = join(" ",$categorias);
        }
        
        $year = $collCat->get($collCat->indexOf(function($key, $value) {
          return strpos($value, '20');
        })
        );
        $permalink = get_permalink();
        $imagenPremio = get_site_url(null, '/wp-content/uploads/2020/02/') . ($ganador ? 'trofeo.png': 'medalla.png');
        $imagenTitle = $ganador ? 'Winner' : 'Finnalist';
        $alttext   = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
        $thumbnail = get_thumbnail( $width, $height, $classtext, $alttext, $titulo, false, 'Blogimage' );
        $thumb     = $thumbnail["thumb"];
        
        $aux.= <<<"EOD"
        <a id="project-$id" href="$permalink" class="$categorias project-item et_pb_portfolio_item" data-category="transition" >
        <div class="hoverJose project-item-cover" style="background-image: url('$thumb');">
        </div>
        <div style=" border-radius: 0px 0px 10px 10px;
        padding:10px;
        box-shadow: 0px 2px 70px 0px rgba(110,130,208,0.18);
        display:flex;
        background-color: #ffffff;
        min-height:52px;
        align-items:center;
        flex-wrap:wrap;
        ">
       <!-- <div title="$imagenTitle" style="background-image: url($imagenPremio);
          position: absolute;
          bottom: 130px; height: 60px;
          border-radius: 60px;
          width: 75px;
          right: 18px;
          background-size: contain;
          background-position: center;
          background-repeat: no-repeat;">
        </div> -->
          <div style="height:32px;min-height:32px;display:flex;align-items:center;margin-top:10px; width:100%" class="tituloAgencia">
            <h5  style="font-weight:bold;color:#00a19a" class="et_pb_tittle">
            $titulo
            </h5>
          </div>
          <div title="Agency" style="width:100%;margin-top:10px; font-size: 12px; color: #282828">
            $agency
          </div>
         <div style="display:flex;width:100%;">
          <div title="Client" style=";font-size: 12px; color: #777777;flex-grow: 1;">
            $client
          </div>
          <div style="font-size: 12px; color: #777777">
            $year
          </div>
        </div>
        </div>
        
      </a>
EOD;
      }
    }
    wp_reset_postdata();
   
    return $aux.'</div>
    <div id="noResults" class="hidden">
    <div style="width:100%;display:flex;justify-content:center;flex-wrap:wrap;">
      <div style="width:100%;display:flex;justify-content:center;flex-wrap:wrap;">
      <img height="200"  src="'.get_site_url(null, '/wp-content/uploads/2020/02/').'noresults.jpg">
      </div>
      <div style="width:100%;text-align: center;" class="et_pb_text_inner"><h1 style="color:#00a19a;">No results found</h1></div>
      <div><h2 style="color:#ccc;font-size:15px;margin-top:10px">Try changing the filters</h2></div>
    </div>
  </div>';
  }
  function getsrc( $image ) {
    if (preg_match('/<img.+?src(?:)*=(?:)*[\'"](.*?)[\'"]/si', $image, $arrResult)) {
      return $arrResult[1];
    }
    return '';
  }
  function overtek_member_filtered_list_shortcode($atts = [], $content = null, $tag = '')
  {
      inicializar();
      wp_enqueue_script('filtered_js', '/wp-content/plugins/overtek-filtered-list/filtered-list.js', ['isotope', 'cells_by_row_js'],null);
      $include_categories = get_term_by('name','members','project_category')->term_id;
      $show_pagination = 'on';
      $posts_number = '-1';
      $fullwidth = 'on';
      $query = ofl_get_members( array(
        'show_pagination'    => $show_pagination,
        'posts_number'       => $posts_number,
        'include_categories' => $include_categories,
        'fullwidth'          => $fullwidth
      ));

      // Format portfolio output, and add supplementary data
      $width     = 'on' === $fullwidth ?  1080 : 400;
      $width     = (int) apply_filters( 'et_pb_portfolio_image_width', $width );
      $height    = 'on' === $fullwidth ?  9999 : 284;
      $height    = (int) apply_filters( 'et_pb_portfolio_image_height', $height );
      $classtext = 'on' === $fullwidth ? 'et_pb_post_main_image' : '';
      
      $aux = '<div style="justify-content:space-around;display:flex;flex-wrap:wrap; margin-bottom: 30px">';
      $aux .= selectAgencias($query);
      $aux .= selectPaises($query);
      $aux .= multiSelectCategorias($query).'</div>';
      um_fetch_user( get_current_user_id() );
      // PINTAR GRID
      $aux .= '<div id="members-container"class="grid">';
      if( $query->have_posts() ) {
        while ( $query->have_posts() ) {
          $query->the_post();
          $id = get_the_ID();
          $titulo = get_the_title();
          $permalink = get_permalink();
          $alttext   = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
          $thumbnail = get_thumbnail( $width, $height, $classtext, $alttext, $titulo, false, 'Blogimage' );
          $thumb     = $thumbnail["thumb"];
          um_fetch_user(get_post_meta(get_the_ID(),'member_id', true));
          $clases = '';
          $claseExtra2 = '';
          $clasePais = preg_replace("/[^\w]/",'', um_user('country'));
          $clases = um_user('Specialisms_36');
          $photo = um_user('cover_photo');
          $cover = getsrc($photo);
          $rutaLogo = get_site_url(null, '/wp-content/uploads/2020/02/').'logoBig.png';
          if ($clases) {
            $clases = array_map(function ($specialism) {
              return preg_replace("/[^\w]/",'',$specialism);
            }, $clases);
            $claseExtra2 = join(" ",$clases);
          }
          $aux.= <<<"EOD"
          <a id="post-$id" href="$permalink" class="$claseExtra2 $clasePais member-item post-$id project  et_pb_portfolio_item " data-category="transition">
            <div class="hoverJose member-item-cover" style="background-position: center;background-image: url('$cover');">
            </div>
           <div style="background-image: url($rutaLogo);position: absolute;bottom: 9px; height: 75px;border-radius: 50px;width: 75px;left: 18px;
            background-size: contain;
            background-position: center;
            background-repeat: no-repeat;"></div>
            <div style="background-color:white;position: absolute;bottom: 12px; height: 69px;border-radius: 50px;width: 69px;left: 21px;
            background-size: contain;
            background-position: center;
            background-repeat: no-repeat;"></div>
            <div style="    width: 60px;
            height: 60px;
            background: white;
            border-radius: 50px;
            bottom: 17px;
            background-image: url($thumb);
            left: 26px;
            background-size: contain;
            background-position: center;
            background-repeat: no-repeat;
            border-color: beige;
            position: absolute;">
            </div>
          
          <div style=" border-radius: 0px 0px 10px 10px;
            padding:5px 10px 5px 100px;
            box-shadow: 0px 2px 70px 0px rgba(110,130,208,0.18);
            justify-content:space-around;display:block;
            background-color: #ffffff;
            align-items:center;">
              <div style="height:32px;min-height:32px;display:flex;align-items:center;margin-top:5px" class="tituloAgencia">
              <h5  style="font-weight:bold;padding-bottom:5px" class="et_pb_tittle">
              $titulo
              </h5>
              </div>
              <div style="margin-bottom:5px" class="et_pb_module et_pb_blurb et_pb_blurb_0  et_pb_text_align_left  et_pb_blurb_position_left et_pb_bg_layout_light">
                <div class="et_pb_blurb_content">
                  <div style="width:20px" class="et_pb_main_blurb_image"><span  class="et_pb_image_wrap"><span  class="et-waypoint et_pb_animation_top et-pb-icon et-animated" style="color: #00a19a;font-size:15px"></span></span></div>
                  <div class="et_pb_blurb_container" style="padding-left:0">
                    <h4 style="font-size:14px;padding-bottom:0;margin-top:0" class="et_pb_module_header"><span>$clasePais</span></h4>
                    
                  </div>
                </div> <!-- .et_pb_blurb_content -->
              </div>
			      </div>
          </a>
EOD;
        }
      }
      wp_reset_postdata();
      
      // override default attributes with user attributes
      return $aux.'</div>
      <div id="noResults" class="hidden">
        <div style="width:100%;display:flex;justify-content:center;flex-wrap:wrap;">
          <div style="width:100%;display:flex;justify-content:center;flex-wrap:wrap;">
          <img height="200"  src="'.get_site_url(null, '/wp-content/uploads/2020/02/').'noresults.jpg">
          </div>
          <div style="width:100%;text-align: center;" class="et_pb_text_inner"><h1 style="color:#00a19a;">No results found</h1></div>
          <div><h2 style="color:#ccc;font-size:15px;margin-top:10px">Try changing the filters</h2></div>
        </div>
			</div>';
    }

    

    add_shortcode('overtek_member_area', 'overtek_member_area_shortcode');
    add_shortcode('overtek_member_filtered_list', 'overtek_member_filtered_list_shortcode');
    add_shortcode('overtek_projects_filtered_list', 'overtek_projects_filtered_list_shortcode');
    add_shortcode('overtek_activities_filtered_list', 'overtek_activities_filtered_list_shortcode');
}
add_action('init', 'overtek_filtered_list_init');