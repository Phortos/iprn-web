
function pintarCabecera (id) {
  var titulos = ['In this area of the website reserved for members you will find all you need about IPRN', 'All members are welcome to participate in those they are interested. This is the best way to get involved in the life and development of the organization, collaborate and benefit from experienced colleagues and intercultural exchange, also is the best way to help IPRN progress, learn how we work and involved your talent staffs. Get in contact with the chairman or Luis to participate or to give any suggestion that you think can be interesting. Committees report also at the AGM and periodically inform the membership about their progress and activities.','Welcome to the Agency Support Resource Centre. This area of the IPRN website reserved exclusively for members contains important and useful information material, documents, presentations, tools and articles that will help you and your agency to continue growing, become more efficient and develop more business.',
'In this section you will be able to donwload every year AGM Minutes', 'Resources of IPRN for members, ready to use them']
  jQuery('.cabeceraGeneral').html(titulos[id]);
  console.log('que pasa');
}
function pintarSeccionIzquierda (id) {
  var elementosDocuments = [
    {
      'tituloSeccion': 'Important membership documents',
      'descripcionSeccion': '',
      'elementos': [
        {
          'url': 'http://iprn.com/wp-content/uploads/2019/10/IPRN-in-Short.pdf',
          'texto': 'IPRN in short',
          'clases': 'normal'
        },
        {
          'url': 'http://iprn.com/wp-content/uploads/2019/01/IPRN-EXPENSES-2018-BUDGET-2019.pdf',
          'texto': 'Budget 2019',
          'clases': 'normal'
        },
        {
          'url': 'http://www.iprn.com/wp-content/uploads/docs-members-area/Guidelines-for-Professional-Conduct.pdf',
          'texto': 'Guidelines for Professional Conduct',
          'clases': 'normal'
        },
        {
          'url': '//docs.google.com/viewer?url=http://www.iprn.com/wp-content/uploads/docs-members-area/IPRN_Business_Plan_2017-2020.ppt&embedded=true&hl=en',
          'texto': 'Business Plan 2017-2020',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2017/11/Planning-the-IPRN-Annual-Meeting-1.doc',
          'texto': 'Planning the Annual Meeting & Conference',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com.customers.tigertech.net/wp-content/uploads/2017/11/Presentation-IPRN.ppt',
          'texto': 'Presentation IPRN',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com.customers.tigertech.net/wp-content/uploads/2017/11/Subscriptions-2018.doc',
          'texto': 'Annual Subscriptions 2018',
          'clases': 'normal'
        },
        {
          'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2017/11/Financial-Statements-IPRN-2017.pdf',
          'texto': 'IPRN Financial Statements 2017',
          'clases': 'normal'
        },
        {
          'url': '//docs.google.com/viewer?url=http://www.iprn.com/wp-content/uploads/docs-members-area/IPRN-Membership-Benefits.doc&embedded=true&hl=en',
          'texto': 'Membership Benefits',
          'clases': 'normal'
        },
        {
          'url': '//docs.google.com/viewer?url=http://www.iprn.com/wp-content/uploads/docs-members-area/How-IPRN-Works.doc&embedded=true&hl=en',
          'texto': 'How IPRN Works',
          'clases': 'normal'
        },
        {
          'url': '//docs.google.com/viewer?url=http://www.iprn.com/wp-content/uploads/docs-members-area/IPRN-CORE-VALUES-AND-CULTURE.doc&embedded=true&hl=en',
          'texto': 'IPRN Core Values and Culture',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com.customers.tigertech.net/wp-content/uploads/2017/11/IPRN-2018-2019-President-Executive-Council-Board-and-Structure-May-2018-1.doc',
          'texto': 'Board, CEO – Gen Sec and Committees',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2017/11/CONSTITUTION-2016.doc',
          'texto': 'Constitution',
          'clases': 'normal'
        }
      ]
    },
    {
      'tituloSeccion': 'Documents for Members use & information',
      'descripcionSeccion': '',
      'elementos': [
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/10/Minutes-of-the-Executive-Council-in-Warsaw-18th-May-2019.doc',
          'texto': 'Minutes of the Executive Council in Warsaw 2019',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/06/Minutes-24th-AGM-Warsaw-2019.doc',
          'texto': 'Minutes AGM Warsaw 2019',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/04/AGM-WARSAW-AGENDA-25-03-2019.xls',
          'texto': 'AGM Warsaw 2019 Agenda',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/03/IPRN-Member-emails-20th-Feb-2nd-2020.doc',
          'texto': 'IPRN Member Emails. Febrero 2020',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/03/IPRN-Member-List-20th-Feb-2nd-2020.doc',
          'texto': 'IPRN Member List. Febrero 2020',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/02/General-Secretary-Report-IPRN-European-Meeting-Luxembourg.docx',
          'texto': 'General Secretary Report IPRN European Meeting Luxembourg',
          'clases': 'normal'
        },
        {
          'url': 'http://iprn.com/wp-content/uploads/2019/02/AGM-Warsaw-2019.pdf',
          'texto': 'AGM Warsaw 2019',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/01/Rules-IPRN-Annual-Awards-2019-Dec.doc',
          'texto': 'Rules IPRN Annual Awards 2019',
          'clases': 'normal'
        },
        {
          'url': 'http://iprn.com/wp-content/uploads/2018/10/IPRN_European_Meeting_Luxembourg_2018.pdf',
          'texto': 'IPRN European Meeting Luxembourg 2018',
          'clases': 'normal'
        },
        {
          'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/Gen-Sec-Report-and-Knowing-IPRN-at-AGM-Beijing-1.pdf',
          'texto': 'Gen Sec Report and Knowing IPRN at AGM Beijing',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com.customers.tigertech.net/wp-content/uploads/2017/11/List-of-AGMs-Cities-y-PotYs.doc',
          'texto': 'List of AGMs Cities and Potys',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com.customers.tigertech.net/wp-content/uploads/2017/11/Minutes-IPRN-European-meeting-2017-Porto.doc',
          'texto': 'Minutes IPRN meeting 2017 Porto',
          'clases': 'normal'
        },
        {
          'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2017/11/Beijing-2018-IPRN-Annual-Meeting-Rundown.pdf',
          'texto': 'Beijing 2018 IPRN Annual Meeting Rundown',
          'clases': 'normal'
        },
        {
          'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/04/Beijing-2018-IPRN-Annual-Meeting-proposal_1104.pdf',
          'texto': 'Presentation Beijing 2018 AGM',
          'clases': 'normal'
        },
        {
          'url': 'http://www.iprn.com/wp-content/uploads/docs-members-area/Company-Information-Sheet-2016.pdf',
          'texto': 'Company Information Sheet',
          'clases': 'normal'
        },
        {
          'url': '//docs.google.com/viewer?url=http://www.iprn.com/wp-content/uploads/docs-members-area/IPRN-MARKETS-SKILLS-2016.docx&embedded=true&hl=en',
          'texto': 'Markets & Skills Matrix',
          'clases': 'normal'
        },
        {
          'url': '//docs.google.com/viewer?url=http://www.iprn.com/wp-content/uploads/docs-members-area/Presentacion_IPRN_Website_Norlan.ppt&embedded=true&hl=en',
          'texto': 'Website',
          'clases': 'normal'
        },
        // {
        //   'url': 'http://www.iprn.com/wp-content/uploads/2012/06/IPRN-contract-between-members.doc',
        //   'texto': 'Contract proposal between members',
        //   'clases': 'normal'
        // },
        {
          'url': '//docs.google.com/viewer?url=http://www.iprn.com/wp-content/uploads/docs-members-area/Template_for_business_between_agencies.doc&embedded=true&hl=en',
          'texto': 'Business between agencies template',
          'clases': 'normal'
        },
        {
          'url': '//docs.google.com/viewer?url=http://www.iprn.com/wp-content/uploads/docs-members-area/Survey_members_2017.doc&embedded=true&hl=en',
          'texto': 'Members Survey 2017',
          'clases': 'normal'
        },
        {
          'url': 'http://www.iprn.com/wp-content/uploads/docs-members-area/WHAT-MEMBERS-WANT-FROM-IPRN-2016.pdf',
          'texto': 'What Members Want from IPRN',
          'clases': 'normal'
        }
      ]
    }
    ];

  var elementosCommittee = [
    {
      'tituloSeccion': 'MEDIA RELATIONS',
      'descripcionSeccion': 'This committee produces IPRN Press Releases, articles and stories of interest to our targets and newsworthy for the news media, with the objective of building the visibility, notoriety, credibility and reputation of IPRN globally. The help of members to translate, adapt and disseminate the news is very important to achieving clippings and results around the globe, where we have members and where we are seeking members',
      'elementos': [
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/Media-Relations-update-May-2018.docx',
          'texto': 'Media Relations update May 2018',
          'clases': 'normal'
        },
        {
          'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/AGM-2018-Media-Relations-Committee_Presentation-at-Beijing.pdf',
          'texto': 'AGM 2018 Media Relations Committee Presentation at Beijing',
          'clases': 'normal'
        }
      ]
    },
    {
      'tituloSeccion': 'NEWSLETTER',
      'descripcionSeccion': '',
      'elementos': [
        {
          'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/07/E-Newsletter-Presentation_Sarah-McOnie.pdf',
          'texto': 'IPRN Newsletter – Sarah McOnie – McOnie Agency – May 2018',
          'clases': 'normal'
        }
      ]
    },
    {
      'tituloSeccion': 'ANNUAL AWARDS',
      'descripcionSeccion': '',
      'elementos': [
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/03/Rules-IPRN-Annual-Awards-Feb-2019.doc',
          'texto': 'Rules IPRN Annual Awards 2019',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/10/Project-of-the-Year-2018-winners-finalists.doc',
          'texto': 'IPRN PROJECT OF THE YEAR 2018 Finalists and Winners',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/10/Main-sponsorship-benefits-POTY.doc',
          'texto': 'Main sponsorship Benefits of the IPRN Project of the Year',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/10/List-of-AGMs-Cities-y-PotYs.doc',
          'texto': 'IPRN AGMs / Conferences and POTYs',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/10/IPRN-Project-of-the-Year-Bursaries-and-Young-Emerging-Leader-award.doc',
          'texto': 'IPRN Project of the Year winners’ Bursaries',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/07/IPRN-Awards-in-China-rock-music-robots-and-hurricanes.docx',
          'texto': 'IPRN Awards in China: rock, music, robots and hurricanes – Jörg Pfanenberg / Dennis Lüneburger – JP Kom – May 2018',
          'clases': 'normal'
        }
      ]
    },
    {
      'tituloSeccion': 'SSMM',
      'descripcionSeccion': '',
      'elementos': [
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/07/Facebook.-Anishkaa-Gehani-Yardstick-1.pptx',
          'texto': 'Facebook Analysis – Anishkaa Gehani – Yardstick – May 2018',
          'clases': 'normal'
        },
        {
          'url': 'http://www.iprn.com/wp-content/uploads/2012/04/Learn-LinkedIn-from-the-Experts.pdf',
          'texto': 'Learn LinkedIn from the Experts | Hubspots',
          'clases': 'normal'
        },
        {
          'url': 'http://www.iprn.com/wp-content/uploads/2012/04/smartbook_roi.pdf',
          'texto': 'The SmartBrief Guide to Social Media Return on Investment',
          'clases': 'normal'
        },
        {
          'url': 'http://www.iprn.com/wp-content/uploads/2012/04/Social-Media-Monitoring-in-Fifteen-Minutes-by-Murray-Newlands2.pdf',
          'texto': 'Social-Media-Monitoring-in-Fifteen-Minutes-by-Murray-Newlands',
          'clases': 'normal'
        }
      ]
    },
    {
      'tituloSeccion': 'SURVEYS',
      'descripcionSeccion': 'Using the potential of our members as independent PR international expert entrepreneurs gives IPRN the chance to conduct surveys as one of the best ways to share information, know better the evolution of PR & Communication globally, understand more our agencies and clients work, what our clients demand and many other information of high interesting for our better knowledge and activities, as well as for the media. This will help also to align better our activities.',
      'elementos': [
        {
          'url': 'http://iprn.com/wp-content/uploads/2019/02/IPRN-Survey-Email-FINAL.pdf',
          'texto': 'IPRN Survey McOnie – February 2019',
          'clases': 'normal'
        },
        {
          'url': 'http://iprn.com/wp-content/uploads/2018/11/12-15-Warsaw.pdf',
          'texto': 'Survey AGM Warsaw 2019',
          'clases': 'normal'
        }
      ]
    },
    {
      'tituloSeccion': 'VIDEO',
      'descripcionSeccion': '',
      'elementos': [
        {
          'url': 'http://iprn.com/wp-content/uploads/2018/11/11-30-IPRN_Projection-16-9-Small.pdf',
          'texto': 'New Projects of The Year',
          'clases': 'normal'
        }
      ]
    },
    {
      'tituloSeccion': 'SPONSORSHIP',
      'descripcionSeccion': '',
      'elementos': [
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/02/Sponsorship-opportunities-with-IPRN.-for-companies-institutions-1.doc',
          'texto': 'Sponsorship opportunities with IPRN (for companies & institutions)',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/02/Sponsorship-opportunities-for-IPRN.-The-concept-for-members-1.doc',
          'texto': 'Sponsorship opportunities for IPRN (for members)',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/02/Sponsorship-benefits-Annual-IPRN-Awards.-Resume-1.doc',
          'texto': 'Sponsorship benefits the IPRN Annual Awards (Resume)',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/02/Sponsoring-the-IPRN-Annual-Awards.-Benefits-complete-1.doc',
          'texto': 'Sponsorship benefits the IPRN Annual Awards (Complete)',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/02/Sponsoring-the-IPRN-Annual-Awards-Example-1.doc',
          'texto': 'Sponsoring the IPRN Annual Awards (Example)',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/02/Example-of-an-IPRN-sponsor-agreement-1.doc',
          'texto': 'Example of an IPRN sponsor agreement',
          'clases': 'normal'
        }
      ]
    }
    ];
  var elementosAI = [
      {
        'tituloSeccion': 'LAST EVENTS',
        'descripcionSeccion': 'European Regional Meeting London 2019 Presentations',
        'elementos': [
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/12/iprnbrand.pdf',
            'texto': 'IPRN Brand',
            'clases': 'normal'
          },
          {
            'url': 'http://www.lucacom.com/iprn/iprnlondon.pdf',
            'texto': 'IPRN European Regional Meeting London',
            'clases': 'normal'
          },
          {
            'url': 'http://www.lucacom.com/iprn/redwood.pdf',
            'texto': 'The changing media landscape and where PR comes in. Redwood Consulting',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/12/spoof-tv.mp4',
            'texto': 'Spoof TV',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/12/W7Worldwide-Presentation.pdf',
            'texto': 'W7Worldwide Presentation',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/12/The-House-PR-Presentation.pdf',
            'texto': 'The House PR Presentation',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/12/Extrovert-Presentation.pdf',
            'texto': 'Extrovert Presentation',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/12/Corporate-Publishing.-What-Next.-Binsfeld.pdf',
            'texto': 'Corporate Publishing. What next? Binsfeld',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/11/How-can-we-use-Big-Data-in-PR.-TDUB.pdf',
            'texto': 'How can we use “Big Data” in PR? TDUB',
            'clases': 'normal'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/11/Brexit-presentation.pptx',
            'texto': 'Brexit Presentation',
            'clases': 'normal'
          }
        ]
      },
      {
        'tituloSeccion': 'AGM Warsaw Presentations',
        'descripcionSeccion': '',
        'elementos': [
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/06/INC-for-IPRN-_-Client-Video-_-Video-Annual-Award-UNAITALIA.pptx',
            'texto': '#GiveTheFloorToAviculturists. INC',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/AGM-Lisboa.pdf',
            'texto': 'AGM Lisbon',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/Promoting-the-Philippines.-Comunicaci%C3%B3n-Iberoamericana.pdf',
            'texto': 'Promoting the Philippines. Comunicación Iberoamericana',
            'clases': 'normal'
          },
          {
            'url': 'http://www.lucacom.com/IPRN/storyseekers.pdf',
            'texto': 'Story Seekers. Magdalena Petryniak',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/Video-did-not-kill-the-PR-Star.-Central-de-Informaçao.pdf',
            'texto': 'Video (did not) kill the PR star. Central de Informaçao',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/The-Changing-Trend-of-PR-Globally.-McOnie.pdf',
            'texto': 'The changing trend of PR globally. McOnie',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/Winning-PR-Marketing-Elements-of-the-Entertainment-Industry-HV.pdf',
            'texto': 'Winning PR & Marketing element of the entertainment industry (H&V)',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/US-Trends.-Media-Relations-and-agency-engagement.-Akhia.pdf',
            'texto': 'US Trends. Media Relations and agency engagement. Akhia',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/Bots-vs-Brain.-All-in-content-creation.-Presigno.pdf',
            'texto': 'Bots vs Brain. All in content creation? Presigno',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/Wribot-Intelligently.-Blue-Focus.pdf',
            'texto': 'Wribot Intelligently. Blue Focus',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/IPRN-Message-Planning.-Channel-Media.pdf',
            'texto': 'IPRN Message Planning. Channel V Media',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/Latin-America-Overview.-Regional-Chair-Latam-Presentation.-High-Results.pdf',
            'texto': 'Latin America Overview. Regional Chair Latam Presentation. High Results',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/PP-AGM-Gen-Sec-Report-IPRN-at-Warsaw-15-May.pdf',
            'texto': 'General Secretary Report. AGM Warsaw',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/E-Newsletter-Analysis.-McOnie.pdf',
            'texto': 'E-Newsletter Analysis. McOnie',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/Strategic-Plan-2020-2025.-Luis.pdf',
            'texto': 'Strategic Plan 2020-2025',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/IPRN-Media-Relations-Committee-Report.-Akhia.pdf',
            'texto': 'IPRN Media Relations Committee Report. Akhia',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/True-Relazioni-Pubbliche.pdf',
            'texto': 'True Relazioni Pubbliche',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/Pr-Partner.-Your-competent-partner-in-Estonia.pdf',
            'texto': 'PR Concept. Your competent partner in Estonia',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/CrestCommunications-presentation-agm.pdf',
            'texto': 'Crest Communications. Presentation',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/Pr-in-India.-Catalyst.pdf',
            'texto': 'PR in India. Catalyst',
            'clases': 'normal'
          },
          {
            'url': 'http://www.lucacom.com/IPRN/enterestoniaprconcept.pdf',
            'texto': 'Enter e-Estonia. The coolest digital society. Pr Concept',
            'clases': 'normal'
          }
        ]
      },
      {
        'tituloSeccion': 'AGM Warsaw Videos',
        'descripcionSeccion': '',
        'elementos': [
          {
            'url': 'http://www.lucacom.com/iprn/yourpartnerinluxembourgbibnsfeld.mp4',
            'texto': 'Your partner in Luxembourg, Binsfeld',
            'clases': 'normal'
          },
          {
            'url': 'http://www.lucacom.com/iprn/iprninstitutionalvideo.mp4',
            'texto': 'IPRN Institutional Video',
            'clases': 'normal'
          },
          {
            'url': 'http://www.lucacom.com/iprn/aldrovcrest.mov',
            'texto': 'Aldrov. Crest Communications',
            'clases': 'normal'
          },
          {
            'url': 'http://www.lucacom.com/iprn/rapalcaseh&v.mp4',
            'texto': 'Rapal Case. H&V',
            'clases': 'normal'
          },
          {
            'url': 'http://www.lucacom.com/iprn/ibismusiclivesherepublicdialog.mp4',
            'texto': 'Ibis Music Lives Here. Public Dialog',
            'clases': 'normal'
          },
          {
            'url': 'http://www.lucacom.com/iprn/superbockarena1central.mp4',
            'texto': 'Super Bock Arena (I). Central de Informaçao',
            'clases': 'normal'
          },
          {
            'url': 'http://www.lucacom.com/iprn/superbockarena2.mp4',
            'texto': 'Super Bock Arena (II). Central de Informaçao',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/EVERCOM-norvento-Corporate-Video-2018.mp4',
            'texto': 'Norvento Corporate Video 2018. Evercom',
            'clases': 'normal'
          },
          {
            'url': 'http://www.lucacom.com/iprn/alanaconsultores.mp4',
            'texto': 'Alana Consultores de Comunicación',
            'clases': 'normal'
          },
          {
            'url': 'http://www.lucacom.com/iprn/imperialgouchaumbrella.mp4',
            'texto': 'Imperial Goucha Umbrella',
            'clases': 'normal'
          },
          {
            'url': 'http://www.lucacom.com/iprn/lopitoileanahowie.mp4',
            'texto': 'Lopito Ileana & Howie',
            'clases': 'normal'
          },
          {
            'url': 'http://www.lucacom.com/iprn/sharelisboa.mp4',
            'texto': '#ShareLisboa. Turismo de Lisboa',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/Wribot-Intelligently.-Blue-Focus.mp4',
            'texto': 'Wribot Intelligently. Blue Focus',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/Uma-noite-no-futuro.-Central-de-Informacao.mp4',
            'texto': 'Uma noite no futuro. Central de Informaçao',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/IPRN-Institutional-Video.-Short.mp4',
            'texto': 'IPRN Institutional Video. Short',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/Estonia.-PR-Concept.mp4',
            'texto': 'Estonia. PR Concept',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/No-Drinkhaton.-PR-Concept.mp4',
            'texto': 'No Drinkhaton. PR Concept',
            'clases': 'normal'
          }
        ]
      },
      {
        'tituloSeccion': 'AGM Beijing Presentations',
        'descripcionSeccion': '',
        'elementos': [
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/07/Social-Media-Trends-in-China.-Blue-Focus-reducido.pdf',
            'texto': 'Social Media & Trends in China. Blue Focus',
            'clases': 'normal'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/07/IPRN-Awards-in-China-rock-music-robots-and-hurricanes.docx',
            'texto': 'IPRN Awards in China: rock, music, robots and hurricanes – Jörg Pfanenberg / Dennis Lüneburger – JP Kom – May 2018',
            'clases': 'normal'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/07/Facebook.-Anishkaa-Gehani-Yardstick-1.pptx',
            'texto': 'Facebook Analysis – Anishkaa Gehani – Yardstick – May 2018',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/07/E-Newsletter-Presentation_Sarah-McOnie.pdf',
            'texto': 'IPRN Newsletter – Sarah McOnie – McOnie Agency – May 2018',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/07/Gaining-a-competitive-edge-in-mature-market.-JP-Kom.pdf',
            'texto': 'Gainig a competitive edge in mature market. Jp Kom',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2018/06/Latin-America-Overview-Sonia-Quesada-High-Results.pdf',
            'texto': 'Latin America Overview – Sonia Quesada – High Results',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/Social-Media-in-South-Africa-and-Africa-Nicole-Capper-Mango.pdf',
            'texto': 'PR Trends in Africa – Nicole Capper – Mango',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2018/06/Branded-Content_E.Life-Rodrigo-Viana-Central-de-Informaçao.pdf',
            'texto': '“Branded Content E_Life”- Rodrigo Viana – Central de Informaçao',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/BFI-Brief-Introduction-2018-Rebeca-Li-Blue-Focus.pdf',
            'texto': 'Blue Focus International. 2018 Presentation – Rebecca Li – Blue Focus',
            'clases': 'normal'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/Media-Relations-update-May-2018.docx',
            'texto': 'Media Relations update May 2018',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/AGM-2018-Media-Relations-Committee_Presentation-at-Beijing.pdf',
            'texto': 'AGM 2018 Media Relations Committee Presentation at Beijing',
            'clases': 'normal'
          }
        ]
      },
      {
        'tituloSeccion': 'European Regional Meeting Luxembourg Presentations',
        'descripcionSeccion': '',
        'elementos': [
          {
            'url': 'http://iprn.com/wp-content/uploads/2018/12/European-Meeting-2018-Agenda.pdf',
            'texto': 'European Meeting 2018 Agenda',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2018/12/IPRN-Corporate-Identity-European-Meeting-2018.pdf',
            'texto': 'Binsfeld brainstorming for IPRN Regional Meeting Luxembourg',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2018/11/11-30-IPRN_Projection-16-9-Small.pdf',
            'texto': 'New Projects of The Year',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2018/11/14-10-OxygenPartners-FE-.pdf',
            'texto': 'Creating momentum in financial and investor education in Luxembourg',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2018/11/12-55-Whats-new-at-HV.pdf',
            'texto': 'What new at H&V?',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2018/11/12-35-McOnie-Re-brand-Presentation.pdf',
            'texto': 'McOnie Re-brand Presentation',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2018/11/12-15-Warsaw.pdf',
            'texto': 'Survey AGM Warsaw 2019',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2018/11/11-20-L45_Agency-overview-_1118.pdf',
            'texto': 'L45 Agency Presentation',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2018/11/11-10-INC-for-IPRN-_-Meeting-Luxembourg-Nov-9.pdf',
            'texto': 'INC Agency Presentation',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2018/11/11-00-2018_TDUB_Agency_final.pdf',
            'texto': 'TDUB Agency Presentation',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2018/11/ispace-Presentation-Antoine-Bocquier.pdf',
            'texto': 'ispace Presentation – Antoine Bocquier',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2018/11/14-00-Journalism-the-digital-era_L45_2018.pdf',
            'texto': 'Journalism in the digital era',
            'clases': 'normal'
          }
        ]
      },
      {
        'tituloSeccion': 'BURSARIES',
        'descripcionSeccion': 'The principal objectives of Bursaries are reward the best and winning entries in the IPRN Annual Awards competition held at each Conference, encourage more entries of a high standard in the competition, and provide the financial means, in whole or in part, for knowledge transfer to selected younger members of the winning agencies to advance their PR experience in their home countries or in other countries.',
        'elementos': [
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/02/038_19-V%C3%ADdeo-Proposal_IPRN.pdf',
            'texto': 'Video Proposal IPRN Central de Informaçao',
            'clases': 'normal'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/02/IPRN-Project-of-the-Year-Winner-Bursary-request.docx',
            'texto': 'IPRN Global Survey – “The Changing Trends of PR Globally – An Agency’s Perspective” – The McOnie Agency',
            'clases': 'normal'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/02/Rules-IPRN-Project-of-the-Year-2018-8-Nov.doc',
            'texto': 'IPRN Project of the Year winner’ Bursaries',
            'clases': 'normal'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/02/IPRN-Training-Day-Peter-Haddock-FINAL.doc',
            'texto': 'PRN: blogging and video training day with Peter Haddock',
            'clases': 'normal'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/02/Bursary-IPRN-staff-exchange-report.doc',
            'texto': 'Bursary IPRN staff exchange report',
            'clases': 'normal'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/02/Bursary-Application-The-McOnie-Agency-2016.doc',
            'texto': 'Bursary application The McOnie Agency 2016',
            'clases': 'normal'
          },
          {
            'url': '//docs.google.com/viewer?url=http://www.iprn.com/wp-content/uploads/2012/04/Bursary-Winners-2011-Report.doc&embedded=true&hl=en',
            'texto': 'Bursary Winner’s 2011 Report',
            'clases': 'normal'
          }
        ]
      },
      {
        'tituloSeccion': 'BEST PRACTICES',
        'descripcionSeccion': '',
        'elementos': [
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/07/The-case-of-PR-Campaign-for-Zasport.pdf',
            'texto': 'The case of PR-Campaing of Zasport – PR Partner – May 2018',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/07/Gaining-a-competitive-edge-in-mature-market.-JP-Kom.pdf',
            'texto': 'Gainig a competitive edge in mature market. Jp Kom',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/The-triumph-of-the-patients.-Headline.pdf',
            'texto': 'The triumph of the patients – Headline',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/Preparing-the-Middle-East-for-VAT-Yardstick-Marketing.pdf',
            'texto': 'Preparing the Middle East for VAT – Yardstick Marketing',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/Eat-Out-Mercedes-Benz-Restaurant-Awards-Mango.pdf',
            'texto': 'Eat Out Mercedes Benz Restaurant Awards – Mango',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/bLU-cRU-Sponsorship-Mango.pdf',
            'texto': 'bLU cRU Sponsorship – Mango',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/Will-Robots-steal-our-jobs-Mc-Onie.pdf',
            'texto': 'Will robots steal our jobs? – McOnie',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2018/06/We-rock-fest-Central-de-Informaçao.pdf',
            'texto': 'We Rock Fest – Central de Informaçao',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/Reimagining-a-jelwery-brand-Channel-V-Media.pdf',
            'texto': 'Reimagining a jewelry brand – Channel V Media',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/PR-in-the-Midst-of-a-Hurricane-JPA1.pdf',
            'texto': 'PR in the Midst of a Hurricane – JPA Health Communications',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/Online-Newsroom-for-journalists-and-influencers-JP-Kom.pdf',
            'texto': 'Online newsroom for journalist and influencers – JP-Kom',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/Ibis.-Music-lives-here-Public-Dialog.pdf',
            'texto': 'Ibis Music – Public Dialog',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/Energy-for-Luxembourg-Binsfeld.pdf',
            'texto': 'Energy for Luxembourg – Binsfeld',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/06/Al-Wahda-Mall-a-journey-to-awards-Yardstick-Marketing.pdf',
            'texto': 'All Wahda Mall, a journey to awards – Yardstick Marketing',
            'clases': 'normal'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com.customers.tigertech.net/wp-content/uploads/2017/12/IPRN-Tourism-Expert-Group-Project.doc',
            'texto': 'IPRN Tourism Expert Group Projectg',
            'clases': 'normal'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com.customers.tigertech.net/wp-content/uploads/2017/12/IPRN-Sector-Expert-Groups-Project.docx',
            'texto': 'IPRN Sector Expert Groups Project',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2017/11/7-enseignements-sur-la-strategie-des-grandes-marques-de-luxe.pdf',
            'texto': 'Luxe & social media: 7 enseignements sur le stratégie des grandes marques',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2017/11/IPRN-Porto-ESCH2022-EYCH2018.pdf',
            'texto': 'IPRN Porto ESCH 2022 & ESCH 2018',
            'clases': 'normal'
          },
          {
            'url': 'http://WWW.IPRN.COM/wp-content/uploads/docs-best/IPRN_presentation.pdf',
            'texto': 'It’d be trite, but true, to say our world has changed | Jonathan',
            'clases': 'normal'
            
          },
          {
            'url': 'http://WWW.IPRN.COM/wp-content/uploads/docs-best/PRPartner_IPRN_final.pdf',
            'texto': 'Russian PR and media trends2016-2017 | PRPartner',
            'clases': 'normal'
          },
          {
            'url': '//docs.google.com/viewer?url=http://WWW.IPRN.COM/wp-content/uploads/docs-best/Marketing_Presentation_wide.pptx&embedded=true&hl=en',
            'texto': 'Blogger & Influencer Engagement, training day | The McOnie Agency',
            'clases': 'normal'
          },
          {
            'url': '//docs.google.com/viewer?url=http://WWW.IPRN.COM/wp-content/uploads/docs-best/IPRN_C._Jones_Collaboration_Monday.pptx&embedded=true&hl=en',
            'texto': 'Landscape Assessment and Stakeholder Mapping | JPA',
            'clases': 'normal'
          }
        ]
      },
      {
        'tituloSeccion': 'ARTICLES',
        'descripcionSeccion': '',
        'elementos': [
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/10/Brand-Fotoprint.docx',
            'texto': 'A global ranking of the most chosen consumer brands',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2019/06/Kantar_-_Brand_Footprint_2019-1.pdf',
            'texto': 'Brand Footprint. Kantar',
            'clases': 'normal'
          },
          {
            'url': 'https://www.jp-kom.de/blog/agentur/agency-management-how-jpkom-creates-value-its-stakeholders-and-gets-competitive-edge',
            'texto': 'How JP│KOM creates value for its stakeholders, and gets a competitive edge in the market',
            'clases': 'link'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/10/Social-Media-landscape-China.docx',
            'texto': 'Social Media Landscape in China',
            'clases': 'normal'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2017/11/EACD-European-Communication-Summit-in-Berlin-2018.doc',
            'texto': 'EACD European Communication Summit in Berlin',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/09/W7Worldwide-Hospitality-Advisory-Report-July-2018.pdf',
            'texto': 'W7Worldwide Hospitality Advisory Report July 2018',
            'clases': 'normal'
          },
          {
            'url': '//docs.google.com/viewer?url=http://www.advertology.ru/article141632.htm&embedded=true&hl=en',
            'texto': 'CEO of IPRN, in the Russian magazine Advertology”',
            'clases': 'normal'
          },
          {
            'url': 'http://www.iprn.com/wp-content/uploads/2015/06/Luis-Canomanuel-WPRF-Discussions.pdf',
            'texto': 'WPRF Discussions – Luis Canomanuel',
            'clases': 'normal'
          }
        ]
      },
      {
        'tituloSeccion': 'AGENCY SPECIAL PROJECTS & PRESENTATIONS',
        'descripcionSeccion': '',
        'elementos': [
          {
            'url': 'http://iprn.com/wp-content/uploads/2020/02/Top-Trends-2020-Evercom-english.pdf',
            'texto': 'Top trends communication 2020',
            'clases': 'normal'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2019/02/IPRN-Training-Day-Peter-Haddock-FINAL.doc',
            'texto': 'PRN: blogging and video training day with Peter Haddock',
            'clases': 'normal'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com.customers.tigertech.net/wp-content/uploads/2017/11/Message-to-IPRN-European-meeting-2017-Porto_Athena_20171104.doc',
            'texto': 'Message to IPRN European Regional Meeting',
            'clases': 'normal'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com.customers.tigertech.net/wp-content/uploads/2017/11/Management-of-IPRN-Blog.ppt',
            'texto': 'Management of IPRN Blog',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2017/11/IPRN_Porto_November_Central.pdf',
            'texto': 'IPRN European Regional Meeting',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2017/11/MONETASSOCIES-2017-Agency-presentation.pdf',
            'texto': 'MONET + ASSOCIÉS 2017 Agency Presentation',
            'clases': 'normal'
          },
          {
            'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com.customers.tigertech.net/wp-content/uploads/2017/11/Presentation-Alana-IV.ppt',
            'texto': 'Identity Presentation – Alana',
            'clases': 'normal'
          },
          {
            'url': 'http://iprn.com/wp-content/uploads/2017/11/Rodrigo-Freitas.-Central-de-Informaçao.pdf',
            'texto': 'New approaches in communication – Central de Informaçao',
            'clases': 'normal'
          },
          {
            'url': '//docs.google.com/viewer?url=http://www.iprn.com/wp-content/uploads/docs-agency-presentations/Warsawa.pptx&embedded=true&hl=en',
            'texto': 'Preview of AGM + Conference Warsaw 2019. | Pawel Bylicki',
            'clases': 'normal'
          },
          {
            'url': '//docs.google.com/viewer?url=http://www.iprn.com/wp-content/uploads/docs-agency-presentations/2.pptx&embedded=true&hl=en',
            'texto': 'Beijing 2018 IPRN Annual Meeting Presentation 0616',
            'clases': 'normal'
          },
          {
            'url': '//docs.google.com/viewer?url=http://iprn.com.customers.tigertech.net/wp-content/uploads/docs-agency-presentations/Beijing_2018_IPRN_Annual_Meeting_Presentation_0616.pptx&embedded=true&hl=en',
            'texto': 'Blue Focus Presentation 2017',
            'clases': 'normal'
          }
        ]
      },
      {
        'tituloSeccion': 'VIDEOS & PHOTOS & INFOGRAPHICS',
        'descripcionSeccion': '',
        'elementos': [
          {
            'url': 'http://www.iprn.com/wp-content/uploads/2015/06/Bob-Zeni-Infographic-Presentation.pdf',
            'texto': 'Infographic Presentation – Bob Zeni',
            'clases': 'normal'
          },
          {
            'url': 'http://www.iprn.com/wp-content/uploads/2015/06/Kelsey-Cox-Value-of-Visualization-Deck-v2.pdf',
            'texto': 'Value of Visualization – Kelsey',
            'clases': 'normal'
          }
        ]
      }
      ];
  var elementosMinutes = [
    {
      'tituloSeccion': 'All minutes',
      'descripcionSeccion': '',
      'elementos': [
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/06/Minutes-23rd-AGM-Beijing-2018.doc',
          'texto': '2018 Beijing AGM Minutes 20.05.2018',
          'clases': 'normal'
        },
        {
          'url': 'http://iprn.com/wp-content/uploads/2017/11/Minutes22ndAGMMoscow2017.pdf',
          'texto': '2017 Moscow AGM Minutes 28.06.2017',
          'clases': 'normal'
        },
        {
          'url': 'http://iprn.com/wp-content/uploads/2018/04/Minutes-21st-AGM-Bordeaux-2016.pdf',
          'texto': '2016 Bordeaux AGM Minutes 27.5.2016',
          'clases': 'normal'
        },
        {
          'url': 'http://view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/04/Washington-DC-USA-AGM-minutes-20-5-2015.doc',
          'texto': '2015 Washington DC AGM minutes 27.5.2015',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/04/2014-London-AGM-minutes-22.-5-.14.doc',
          'texto': '2014 London AGM minutes 22.5.2014',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/04/Istanbul-AGM-minutes-5.5.2013-1.doc',
          'texto': '2013 Istanbul AGM minutes 5.5.2013',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/04/Buenos-Aires-AGM-minutes-22.4.-2012.doc',
          'texto': '2012 Buenos Aires AGM minutes 22.5.2012',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/04/2011-Prague-AGM-minutes-final-16-5-2010.doc',
          'texto': '2011 Prague AGM minutes 16.5.2011',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/04/2010-Toronto-AGM-minutes-16-5-2010-Rev1.doc',
          'texto': '2010 Toronto AGM minutes 16.5.2010',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/04/2009-Milan-AGM-minutes-21.5.2005.doc',
          'texto': '2009 Milan AGM minutes 21.5.2009',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/04/2008-Dubai-AGM-minutes-27.4.2008.doc',
          'texto': '2008 Dubai AGM minutes 27.4.2008',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/04/2007-Zurich-AGM-minutes-.doc',
          'texto': '2007 Zurich AGM minutes',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/04/2006-AGM-minutes-San-Jose-21-May-06.doc',
          'texto': '2006 San Jose AGM minutes 21.5.2006',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/04/2005-AGM-Minutes-Dublin-22.5.2005-doc.doc',
          'texto': '2005 Dublin AGM Minutes 22.5.2005',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/04/2004-Helsinki-AGM-minutes-6.6.2004.doc',
          'texto': '2004 Helsinki AGM minutes 6.6.2004',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/04/2003-Luxembourg-AGM-Minutes.doc',
          'texto': '2003 Luxembourg AGM Minutes',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/04/2002-Argentina-AGM-Minutes.doc',
          'texto': '2002 Argentina AGM Minutes',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/04/2001-MADRID-AGM-minutes.doc',
          'texto': '2001 Madrid AGM minutes',
          'clases': 'normal'
        },
        {
          'url': '//view.officeapps.live.com/op/embed.aspx?src=http://iprn.com/wp-content/uploads/2018/04/2000-Cleveland-AGM-minutes.doc',
          'texto': '2000 Cleveland AGM minutes',
          'clases': 'normal'
        }
      ]
    }
  ];
  var elementosGraphics = [
    {
      'tituloSeccion': 'IPRN LOGOS – JPG FORMAT',
      'descripcionSeccion': 'To download, right click on the image and follow your browser’s link to “save image” to your computer.',
      'elementos': [
        {
          'url': 'http://www.iprn.com/wp-content/uploads/2013/09/IPRN_Black.jpg',
          'texto': 'Black & White – 300 x 101',
          'clases': 'picture'
        },
        {
          'url': 'http://www.iprn.com/wp-content/uploads/2013/09/IPRN.jpg',
          'texto': 'Color logo',
          'clases': 'picture'
        }
      ]
    },
    {
      'tituloSeccion': 'IPRN LOGOS – EPS FORMAT (FOR PROFESSIONAL USE)',
      'descripcionSeccion': 'CMYK and Pantone colour templates. Regular logo template as well as a template to add member country (see example below)',
      'elementos': [
        {
          'url': 'http://www.iprn.com/wp-content/uploads/2013/09/IPRN-Logos-in-EPS-format.zip',
          'texto': 'Zip file',
          'clases': 'link'
        }
      ]
    },
    {
      'tituloSeccion': 'OTHER IPRN GRAPHICS',
      'descripcionSeccion': 'To download, right click on the image and follow your browser’s link to “save image” to your computer.',
      'elementos': [
        {
          'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2013/09/IPRN-Web-Graphic-02.jpg',
          'texto': 'IPRN Map graphic Blue',
          'clases': 'picture'
        },
        {
          'url': 'http://iprn.com.customers.tigertech.net/wp-content/uploads/2018/07/map-white-background.jpg',
          'texto': 'IPRN Map graphic Grey',
          'clases': 'picture'
        },
        {
          'url': 'http://www.iprn.com/wp-content/uploads/2013/09/IPRN-Web-Graphic-01.jpg',
          'texto': 'IPRN “words” graphic',
          'clases': 'picture'
        }
      ]
    }
  ];
    
    var elementosPintar = [];
    var retorno = '';
    switch (id) {
      case 0:
      elementosPintar = elementosDocuments;
      break;
      case 1:
      elementosPintar = elementosCommittee;
      break;
      case 2:
      elementosPintar = elementosAI;
      break;
      case 3:
      elementosPintar = elementosMinutes;
      break;
      case 4:
      elementosPintar = elementosGraphics;
      break;
    }
    elementosPintar.forEach(el => {
      
      retorno += '<div class="seccionTitulo" ><div class="marcador"><div title="'+el.descripcionSeccion+'" class="tituloSeccion"><span>'+el.tituloSeccion+'</span></div></div>'+'<div class="seccionContenido">';
      el.elementos.forEach(doc => {
        retorno += '<div class="documento '+doc.clases+'"><a style="display:inline-flex;width:80%" href="'+doc.url+'">'+doc.texto+'</a><i style="position: relative;right: -37px;" class="fas fa-arrow-alt-circle-right"></i></div>'
      })
      retorno+='</div></div>';
    })
    jQuery('.seccionIzquierda').html(retorno);
}



function pintarTab (id) {
  pintarCabecera(id)
  pintarSeccionIzquierda(id)
}

jQuery( function() {

  pintarTab(0);

  jQuery('.categoryButton').on('click', function() {

    // do something
    if (!jQuery(this).hasClass('activo')) {
      jQuery('.documento').removeClass('activo');
      jQuery('#noDoc').removeClass('desactivado');
      jQuery('#siDoc').addClass('desactivado');
      jQuery('#iFrameDoc').attr("src", '');
      jQuery('.categoryButton').removeClass('activo');
      jQuery(this).addClass('activo');
      switch (jQuery(this).attr('id')) {
        case 'Documents':
          pintarTab(0);
        break;
        case 'Committee':
          jQuery('#contenedorTab').html(pintarTab(1));
        break;
        case 'AI center':
          jQuery('#contenedorTab').html(pintarTab(2));
        break;
        case 'Minutes':
          jQuery('#contenedorTab').html(pintarTab(3));
        break;
        case 'Resources':
          jQuery('#contenedorTab').html(pintarTab(4));
        break;
      }
    }
    
    
    
  });

  jQuery('.seccionIzquierda').on('click', 'a', function(event) {
    // do something
    event.preventDefault()
    event.stopPropagation();
    jQuery('.documento').removeClass('activo');
    jQuery(this).parent().addClass('activo');
    var ruta = jQuery(this).attr('href');
    
    jQuery('#siDoc').removeClass('desactivado');
    jQuery('#noDoc').addClass('desactivado');
    if (jQuery(this).parent().hasClass('link')) {
      console.log('soy link')
      window.location.replace(jQuery(this).attr('href'));
      jQuery('.logo').attr("src", '');
    }
    
    if (jQuery(this).parent().hasClass('picture')) {
      console.log('soy foto')
      jQuery('.logo').attr("src", ruta);
      var rutaDownload = ruta.replace(/.*src=/,'');
      jQuery('.download').attr("href", rutaDownload);
      jQuery('.download').css({'display': 'block'});
      jQuery('#iFrameDoc').removeClass('activo');
      jQuery('.embeb').removeClass('activo');
      jQuery('.logo').addClass('activo');

    }
    if (jQuery(this).parent().hasClass('normal')) {
      console.log('soy normal')
      var rutaDownload = ruta.replace(/.*src=/,'');
      jQuery('.download').attr("href", rutaDownload);
      if (rutaDownload === ruta) {
        jQuery('.download').css({'display': 'none'});
      } else {
        jQuery('.download').css({'display': 'block'});
      }
      
      jQuery('#iFrameDoc').attr("src", ruta);
      jQuery('#iFrameDoc').addClass('activo');
      jQuery('.embeb').removeClass('activo');
      jQuery('.logo').removeClass('activo');
      if (jQuery(window).width() < 770) {
        jQuery( ".contenedorDocumento" ).css({'display': 'block'});
      }
    }
    
    
  });

  jQuery('.seccionIzquierda').on('click', '.seccionTitulo', function() {
    // do something
    if (!jQuery(this).hasClass('activo')) {
      jQuery('.seccionTitulo').removeClass('activo');
      jQuery(this).addClass('activo');
      jQuery('.documento').removeClass('activo');
      jQuery('#noDoc').removeClass('desactivado');
      jQuery('#siDoc').addClass('desactivado');
      jQuery('#iFrameDoc').attr("src", '');
      jQuery('.embeb').attr("src", '');
    } else {
      jQuery('.seccionTitulo').removeClass('activo');
      jQuery('.documento').removeClass('activo');
      jQuery('#noDoc').removeClass('desactivado');
      jQuery('#siDoc').addClass('desactivado');
      jQuery('#iFrameDoc').attr("src", '');
      jQuery('.embeb').attr("src", '');
     
    }
  });
  jQuery('#cerrarButton').on('click', function() {
    event.preventDefault();
    console.log('asdad');
    event.stopPropagation();
    jQuery( ".contenedorDocumento" ).css({'display': 'none'});
  });
  // MOVIL
  if (jQuery(window).width() < 770) {
    jQuery( ".seccionIzquierda" ).css({'width': '100%'});
    jQuery( ".top.categoryButton" ).css({'width': '100%'});
    jQuery( "#cerrarButton" ).css({'display': 'block', 'width' : '40px'});
    jQuery( ".contenedorDocumento" ).css({'width': '100%','position':'absolute', 'display': 'none'});
  }


} );







