   
var itemSelector = ".project-item"; 
var $checkboxes = jQuery('.member-filter');
var $container = jQuery('#projects-container').isotope({  masonry: { gutter:60 }, itemSelector: itemSelector });

//Ascending order
var responsiveIsotope = [ [480, 4] , [720, 4] ];
var itemsPerPageDefault = 12;
var itemsPerPage = defineItemsPerPage();
var currentNumberPages = 1;
var currentPage = 1;
var currentFilter = '*';
var filterAttribute = 'data-filter';
var filterValue = "";
var pageAttribute = 'data-page';
var pagerClass = 'isotope-pager';

// update items based on current filters    
function changeFilter(selector) { $container.isotope({ filter: selector });}



//grab all checked filters and goto page on fresh isotope output
function goToPage(n) {
    currentPage = n;
    var selector = itemSelector;
    var exclusives = [];
    filterValue = '*';
    // add page number to the string of filters
    
    
    filtroActivo = '';
    filtroAnio = jQuery('#selectBoxAnios').val() ? jQuery('#selectBoxAnios').val() : '';
    if (jQuery('#category-all').hasClass('activo')) {
      filtroActivo = '*'+filtroAnio;
    } else {
      if (jQuery('.categoryButton.activo')) {
        jQuery.each(jQuery('.categoryButton.activo'), function (i, val) {
          filtroActivo += '.'+jQuery(this).attr('id').split("-").pop() + filtroAnio + (i != jQuery('.categoryButton.activo').length-1 ? ', ' : '')
        })
      }
    }
    
    filterValue = filtroActivo;
    var wordPage = currentPage.toString();
    filterValue += ('.'+wordPage);
    changeFilter(filterValue);
    if (jQuery('#projects-container').isotope('getFilteredItemElements').length == 0) {
      _desaparecerContainer(true, null, 'noResults');
    } else {
      _desaparecerContainer(false, null, 'noResults');
    }
}

// determine page breaks based on window width and preset values
function defineItemsPerPage() {
    var pages = itemsPerPageDefault;

    for( var i = 0; i < responsiveIsotope.length; i++ ) {
        if( jQuery(window).width() <= responsiveIsotope[i][0] ) {
            pages = responsiveIsotope[i][1];
            break;
        }
    }
    return pages;
}
function _desaparecerContainer (mostrar, borrar, id) {
  // ANIMAR CONTENEDOR
  var contenedor = jQuery('#'+id)[0];
  if (mostrar) {
    contenedor.classList.remove('hidden');
    setTimeout(function () {
      contenedor.classList.remove('visuallyhidden');
    }, 20);
  } else {
    contenedor.classList.add('visuallyhidden');    
    contenedor.addEventListener('transitionend', function(e) {
      if (borrar != null) {
        borrar.remove();
      }
      contenedor.classList.add('hidden');
    }, {
      capture: false,
      once: true,
      passive: false
    });
  }
}

function setPagination() {

    var SettingsPagesOnItems = function(){
        var itemsLength = $container.children(itemSelector).length;
        var pages = Math.ceil(itemsLength / itemsPerPage);
        var item = 1;
        var page = 1;
        var selector = itemSelector;
        var exclusives = [];
        filterValue = '*';
        filtroActivo = '';
        if (jQuery('#category-all').hasClass('activo')) {
          filtroActivo = '*';
        } else {
          if (jQuery('.categoryButton.activo')) {
            jQuery.each(jQuery('.categoryButton.activo'), function (i, val) {
              filtroActivo += jQuery(this).attr('id').split("-").pop() + (i != jQuery('.categoryButton.activo').length-1 ? ' ' : '')
            })
          }
        }
        filtroAnio = jQuery('#selectBoxAnios').val() ? jQuery('#selectBoxAnios').val() : '';
        filterValue = filtroActivo+filtroAnio;
        // find each child element with current filter values
        $container.children(filterValue).each(function(){
          // increment page if a new one is needed
          if( item > itemsPerPage ) {
              page++;
              item = 1;
              
          }
          // add page number to element as a class
          wordPage = page.toString();
          var classes = jQuery(this).attr('class').split(' ');
          var lastClass = classes[classes.length-1];
          // last class shorter than 4 will be a page number, if so, grab and replace
          if(lastClass.length < 4){
            jQuery(this).removeClass();
              classes.pop();
              classes.push(wordPage);
              classes = classes.join(' ');
              jQuery(this).addClass(classes);
          } else {
              // if there was no page number, add it
              jQuery(this).addClass(wordPage); 
          }
          item++;
        });
        currentNumberPages = page;
    }();

    // create page number navigation
    var CreatePagers = function() {

        var $isotopePager = ( jQuery('.'+pagerClass).length == 0 ) ? jQuery('<div class="'+pagerClass+'"></div>') : jQuery('.'+pagerClass);

        $isotopePager.html('');
        if(currentNumberPages > 1){
          for( var i = 0; i < currentNumberPages; i++ ) {
            var activo = i == 0 ? ' activo' : ''
              var $pager = jQuery('<a href="javascript:void(0);" class="pager'+activo+'" '+pageAttribute+'="'+(i+1)+'"></a>');
                  $pager.html(i+1);
                  $pager.click(function(){
                    jQuery('.pager').removeClass('activo');
                    jQuery(this).addClass('activo');
                    var page = jQuery(this).eq(0).attr(pageAttribute);
                    goToPage(page);
                });
            $pager.appendTo($isotopePager);
        }
      }
      $container.after($isotopePager);
  }();
}
setPagination();
goToPage(1);



jQuery(window).resize(function(){
  itemsPerPage = defineItemsPerPage();
  setPagination();
  goToPage(1);
});



jQuery( function() {

  // PAISES

  jQuery.widget( "custom.selectBox", {
    _create: function() {
      this.wrapper = jQuery(`<div id="filtro-`+this.element[0].id+`" style="
      display: inline-block;
      height: 41px;
      width:300px;
      box-sizing: border-box;"">
      </div>`)
        .addClass( "custom-combobox" )
        .attr("placeholder", "Your Text Here")
        .insertAfter( this.element );
      this.element.hide();
      this._createAutocomplete();
      this._createShowAllButton();
      this._createClearAllButton();
    },

    _createAutocomplete: function() {
      var selected = this.element.children( ":selected" ),
        value = selected.val() ? selected.text() : "";
      this.input = jQuery( '<input style="height: 41px;padding-right: 0;cursor: text;border:none;background: transparent;width:75%;margin-bottom:6px;border-bottom: 2px solid #00a19a;border-radius: 4px 4px 0 0;" >' )
        .prependTo( '#filtro-'+this.element[0].id )
        .val( value )
        .attr( "title", "" )
        .attr( "placeholder", this.options.placeholder )
        .addClass( "custom-combobox-input  ui-widget-content ui-state-default ui-corner-left" )
        .autocomplete({
          delay: 0,
          minLength: 0,
          source: jQuery.proxy( this, "_source" )
        })
        .tooltip({
          classes: {
            "ui-tooltip": "ui-state-highlight"
          }
        });

      this._on( this.input, {
        autocompleteselect: function( event, ui ) {
          ui.item.option.selected = true;
          var filter = ui.item.value;
          currentFilter = filter;
          setPagination();
          jQuery('#trash-'+this.element[0].id).button( "option", "disabled", false );
          goToPage(1);
          // this.input.append(nuevoFiltro);
          this._trigger( "select", event, {
            item: ui.item.option
          });
          this.input.val(ui.item.label)
          return false;
        },
        autocompletechange: "_removeIfInvalid"
      });
    },

    _createShowAllButton: function() {
      var input = this.input,
        wasOpen = false;

      jQuery( '<a style="float:right;border-radius: 34px;height: 32px;width: 32px;background: none;margin-top: 5px;border: 1px solid #00a19a;">' )
        .attr( "tabIndex", -1 )
        .attr( "title", "Show All Items" )
        .tooltip()
        .appendTo('#filtro-'+this.element[0].id )
        .button({
          icons: {
            primary: "ui-icon-triangle-1-s"
          },
          text: false
        })
        .addClass( "custom-combobox-toggle" )
        .on( "mousedown", function() {

          wasOpen = input.autocomplete( "widget" ).is( ":visible" );
        })
        .on( "click", function() {
          input.trigger( "focus" );
          // Close if already visible
          if ( wasOpen ) {
            return;
          }

          // Pass empty string as value to search for, displaying all results
          input.autocomplete( "search", "" );
        });
    },
    _createClearAllButton: function() {
      var idElement = this.element[0].id
      var input = this.input,
      
        wasOpen = false;

      jQuery( '<a id="trash-'+idElement+'" style="float:right;border-radius: 34px;height: 32px;width: 32px;background: none;margin-top: 5px;border: 1px solid #00a19a;">' )
        .attr( "tabIndex", -1 )
        .attr( "title", "Delete All Filters" )
        .tooltip()
        .prop("disabled", true)
        .appendTo('#filtro-'+this.element[0].id )
        .button({
          icons: {
            primary: "ui-icon-trash"
          },
          text: false
        })
        .removeClass( "ui-corner-all" )
        .addClass( "custom-combobox-trash ui-corner-top-right" )
        .on( "click", function() {
          jQuery('#'+idElement).val("")
          currentFilter = '*';
          input.val('');
          this.value = "";
          jQuery('#trash-'+idElement).button( "option", "disabled", true);
          setPagination();
          goToPage(1);
        })
    },

    

    _source: function( request, response ) {
      var matcher = new RegExp( jQuery.ui.autocomplete.escapeRegex(request.term), "i" );
      response( this.element.children( "option" ).filter(function () {return this.index != 0 &&  !this.selected}).map(function() {
        var text = jQuery( this ).text();
        if ( this.value && ( !request.term || matcher.test(text) ) )
          return {
            label: text,
            value: this.value,
            option: this
          };
      }) );
    },


    _removeIfInvalid: function( event, ui ) {

      // Selected an item, nothing to do
      if ( ui.item ) {
        return false;
      }

      // Search for a match (case-insensitive)
      var value = this.input.val(),
        valueLowerCase = value.toLowerCase(),
        valid = false;
      this.element.children( "option" ).each(function() {
        
        if ( jQuery( this ).text().toLowerCase() === valueLowerCase ) {
          this.selected = valid = true;
          return false;
        }
      });

      // Found a match, nothing to do
      if ( valid ) {
        return false;
      }

      // Remove invalid value
      this.input
        .val( "" )
        .attr( "title", value + " didn't match any item" )
        .tooltip( "open" );
      this.element.val( "" );
      this._delay(function() {
        this.input.tooltip( "close" ).attr( "title", "" );
      }, 2500 );
      this.input.autocomplete( "instance" ).term = "";
    },

    _destroy: function() {
      this.wrapper.remove();
      this.element.show();
    }
  });
  jQuery('#inputBusqueda').on('input', function() {
    // do something
    setPagination();
    goToPage(1);
  });

  jQuery( "#selectBoxAnios" ).selectBox({placeholder: 'Choose a year'});
  jQuery( "#toggle" ).on( "click", function() {
    jQuery( "#combobox" ).toggle();
  });

  console.log(jQuery(window).width());
  // PC PEQUEÑO
  if (jQuery(window).width() < 1300) {
    jQuery( "#projects-container" ).css({'margin-left': '70px'});
    
  }
  // IPAD TUMBADO
  if (jQuery(window).width() < 1115) {
    jQuery( ".project-item" ).css({'width': '380px'});
    jQuery( "#projects-container" ).css({'margin-left': '35px'});
  }
  // IPAD
  if (jQuery(window).width() < 800) {
    
  }
  // MOVIL
  if (jQuery(window).width() < 770) {
    jQuery( ".project-item" ).css({'width': '430px'});
    jQuery( "#projects-container" ).css({'margin-left': '100px'});
  }
  // MOVIL
  if (jQuery(window).width() < 500) {
    jQuery( "#projects-container" ).css({'margin-left': '5px'});
    jQuery( ".project-item" ).css({'width': '320px'});
  }
  // MOVIL
  if (jQuery(window).width() < 400) {
    jQuery( "#projects-container" ).css({'margin-left': '5px'});
    jQuery( ".project-item" ).css({'width': '280px'});
  }
  jQuery('.categoryButton').on('click', function() {
    if (jQuery(this).attr('id') == 'category-all') {
      if (!jQuery(this).hasClass( 'activo' )) {
        jQuery('.categoryButton').removeClass('activo')
        jQuery(this).addClass('activo');
      }
    } else {
      if (jQuery(this).hasClass( 'activo' )) {
        if (jQuery('.categoryButton.activo').length == 1) {
          jQuery('#category-all').addClass('activo')
        }
        jQuery(this).removeClass('activo')
      } else {
        jQuery(this).addClass('activo');
        jQuery('#category-all').removeClass('activo')
      }
    }
  
    // do something
    setPagination();
    goToPage(1);
  });
})
