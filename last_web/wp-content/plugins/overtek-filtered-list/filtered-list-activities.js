   
var itemSelector = ".activity-item"; 
var $checkboxes = jQuery('.member-filter');
var $container = jQuery('#activities-container').isotope({ itemSelector: itemSelector, masonry: { gutter:5 , columnWidth: 200, horizontalOrder: true } });

//Ascending order
var responsiveIsotope = [ [480, 4] , [720, 4] ];
var itemsPerPageDefault = 12;
var itemsPerPage = defineItemsPerPage();
var currentNumberPages = 1;
var currentPage = 1;
var currentFilter = '*';
var filterAttribute = 'data-filter';
var filterValue = "";
var pageAttribute = 'data-page';
var pagerClass = 'isotope-pager';

// update items based on current filters    
function changeFilter(selector) { $container.isotope({ filter: selector });}



//grab all checked filters and goto page on fresh isotope output
function goToPage(n) {
    currentPage = n;
    var selector = itemSelector;
    var exclusives = [];
    filterValue = '*';
    // add page number to the string of filters
    filtroActivo = '';
    if (jQuery('#category-all').hasClass('activo')) {
      filtroActivo = '*';
    } else {
      if (jQuery('.categoryButton.activo')) {
        jQuery.each(jQuery('.categoryButton.activo'), function (i, val) {
          filtroActivo += '.'+jQuery(this).attr('id').split("-").pop() + (i != jQuery('.categoryButton.activo').length-1 ? ', ' : '')
        })
      }
    }
    
    filterValue = filtroActivo;
    var wordPage = currentPage.toString();
    filterValue += ('.'+wordPage);
    changeFilter(filterValue);
    if (jQuery('#activities-container').isotope('getFilteredItemElements').length == 0) {
      _desaparecerContainer(true, null, 'noResults');
    } else {
      _desaparecerContainer(false, null, 'noResults');
    }
}

// determine page breaks based on window width and preset values
function defineItemsPerPage() {
    var pages = itemsPerPageDefault;

    for( var i = 0; i < responsiveIsotope.length; i++ ) {
        if( jQuery(window).width() <= responsiveIsotope[i][0] ) {
            pages = responsiveIsotope[i][1];
            break;
        }
    }
    return pages;
}
function _desaparecerContainer (mostrar, borrar, id) {
  // ANIMAR CONTENEDOR
  var contenedor = jQuery('#'+id)[0];
  if (mostrar) {
    contenedor.classList.remove('hidden');
    setTimeout(function () {
      contenedor.classList.remove('visuallyhidden');
    }, 20);
  } else {
    contenedor.classList.add('visuallyhidden');    
    contenedor.addEventListener('transitionend', function(e) {
      if (borrar != null) {
        borrar.remove();
      }
      contenedor.classList.add('hidden');
    }, {
      capture: false,
      once: true,
      passive: false
    });
  }
}

function setPagination() {

    var SettingsPagesOnItems = function(){
        var itemsLength = $container.children(itemSelector).length;
        var pages = Math.ceil(itemsLength / itemsPerPage);
        var item = 1;
        var page = 1;
        var selector = itemSelector;
        var exclusives = [];
        filterValue = '*';
        filtroActivo = '';
        if (jQuery('#category-all').hasClass('activo')) {
          filtroActivo = '*';
        } else {
          if (jQuery('.categoryButton.activo')) {
            jQuery.each(jQuery('.categoryButton.activo'), function (i, val) {
              filtroActivo += '.'+jQuery(this).attr('id').split("-").pop() + (i != jQuery('.categoryButton.activo').length-1 ? ', ' : '')
            })
          }
        }
        
        filterValue = filtroActivo;
        // find each child element with current filter values
        $container.children(filterValue).each(function(){
          // increment page if a new one is needed
          if( item > itemsPerPage ) {
              page++;
              item = 1;
              
          }
          // add page number to element as a class
          wordPage = page.toString();
          var classes = jQuery(this).attr('class').split(' ');
          var lastClass = classes[classes.length-1];
          // last class shorter than 4 will be a page number, if so, grab and replace
          if(lastClass.length < 4){
            jQuery(this).removeClass();
              classes.pop();
              classes.push(wordPage);
              classes = classes.join(' ');
              jQuery(this).addClass(classes);
          } else {
              // if there was no page number, add it
              jQuery(this).addClass(wordPage); 
          }
          item++;
        });
        currentNumberPages = page;
    }();

    // create page number navigation
    var CreatePagers = function() {

        var $isotopePager = ( jQuery('.'+pagerClass).length == 0 ) ? jQuery('<div class="'+pagerClass+'"></div>') : jQuery('.'+pagerClass);

        $isotopePager.html('');
        if(currentNumberPages > 1){
          for( var i = 0; i < currentNumberPages; i++ ) {
              var activo = i == 0 ? ' activo' : ''
              var $pager = jQuery('<a href="javascript:void(0);" class="pager'+activo+'" '+pageAttribute+'="'+(i+1)+'"></a>');
                  $pager.html(i+1);
                  $pager.click(function(){
                    jQuery('.pager').removeClass('activo');
                    jQuery(this).addClass('activo');
                    var page = jQuery(this).eq(0).attr(pageAttribute);
                    goToPage(page);
                });
            $pager.appendTo($isotopePager);
        }
      }
      $container.after($isotopePager);
  }();
}
setPagination();
goToPage(1);



jQuery(window).resize(function(){
  itemsPerPage = defineItemsPerPage();
  setPagination();
  goToPage(1);
});



jQuery( function() {

  jQuery('#activities-container').magnificPopup({
    delegate: "a",
    type: "image",
    removalDelay: 500,
    gallery: {
      enabled: true,
      navigateByImgClick: true
    },
    mainClass:"mfp-fade",
    autoFocusLast:false
  })
  console.log(jQuery(window).width());
  // PC PEQUEÑO
  if (jQuery(window).width() < 1300) {
    jQuery( "#activities-container" ).css({'margin-left': '100px'});
  }
  if (jQuery(window).width() < 1115) {
    jQuery( "#activities-container" ).css({'margin-left': '120px'});
  }
  // IPAD
  if (jQuery(window).width() < 800) {
   
  }
  // MOVIL
  if (jQuery(window).width() < 500) {
    jQuery( "#activities-container" ).css({'margin-left': '60px'});
  }
  jQuery('.categoryButton').on('click', function() {
    if (jQuery(this).attr('id') == 'category-all') {
      if (!jQuery(this).hasClass( 'activo' )) {
        jQuery('.categoryButton').removeClass('activo')
        jQuery(this).addClass('activo');
      }
    } else {
      if (jQuery(this).hasClass( 'activo' )) {
        if (jQuery('.categoryButton.activo').length == 1) {
          jQuery('#category-all').addClass('activo')
        }
        jQuery(this).removeClass('activo')
      } else {
        jQuery('.categoryButton').removeClass('activo')
        jQuery(this).addClass('activo');
        jQuery('#category-all').removeClass('activo')
        
      }
    }
    // do something
    setPagination();
    goToPage(1);
  });
})
