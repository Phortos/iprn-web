<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitd35d7e83fa02f0de3bf3ca073032614a
{
    public static $prefixLengthsPsr4 = array (
        'D' => 
        array (
            'Doctrine\\Common\\Collections\\' => 28,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Doctrine\\Common\\Collections\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/collections/lib/Doctrine/Common/Collections',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitd35d7e83fa02f0de3bf3ca073032614a::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitd35d7e83fa02f0de3bf3ca073032614a::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
